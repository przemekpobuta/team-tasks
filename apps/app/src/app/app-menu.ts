import { NbMenuItem } from '@nebular/theme';

export const GUEST_MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Logowanie',
    icon: 'log-in-outline',
    link: '/auth/login',
    pathMatch: 'prefix'
  },
  {
    title: 'Rejestracja',
    icon: 'person-add-outline',
    link: '/auth/register',
    pathMatch: 'prefix'
  }
];

export const USER_MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Skrzynka odbiorcza',
    icon: 'inbox-outline',
    link: '/inbox',
    home: true,
    pathMatch: 'prefix'
  },
  {
    title: 'Projekty',
    icon: 'folder-outline',
    link: '/projects',
    home: true,
    pathMatch: 'prefix'
  },
  {
    title: 'Zespoły',
    icon: 'people-outline',
    link: '/teams',
    pathMatch: 'prefix'
  }
];
