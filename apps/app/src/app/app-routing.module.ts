import { NgModule } from '@angular/core';
import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@team-tasks/app/core/auth';

const routes: Routes = [
  {
    path: 'inbox',
    canActivate: [AuthGuard],
    loadChildren: () => import('@team-tasks/app/feature/inbox').then(m => m.AppFeatureInboxModule)
  },
  {
    path: 'projects',
    canActivate: [AuthGuard],
    loadChildren: () => import('@team-tasks/app/feature/projects').then(m => m.AppFeatureProjectsModule)
  },
  {
    path: 'teams',
    canActivate: [AuthGuard],
    loadChildren: () => import('@team-tasks/app/feature/teams').then(m => m.AppFeatureTeamsModule)
  },
  {
    path: 'auth',
    loadChildren: () => import('@team-tasks/app/feature/auth').then(m => m.AppFeatureAuthModule)
  },
  { path: '', redirectTo: 'auth/login', pathMatch: 'full' },
  { path: '**', redirectTo: '' }
];

const config: ExtraOptions = {
  useHash: false
};

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
