import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AkitaNgDevtools } from '@datorama/akita-ngdevtools';
import { AppCoreAuthModule } from '@team-tasks/app/core/auth';
import { AppCoreLoggerModule } from '@team-tasks/app/core/logger';
import { AppUiLayoutModule } from '@team-tasks/app/ui/layout';

import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

const CORE_MODULES = [
  AppCoreAuthModule.forRoot(),
  AppCoreLoggerModule.forRoot(),
  AppUiLayoutModule.forRoot(),
  environment.production ? [] : AkitaNgDevtools.forRoot()
];

@NgModule({
  declarations: [AppComponent],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    ...CORE_MODULES,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
