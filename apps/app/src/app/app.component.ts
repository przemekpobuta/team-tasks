import { Component, OnInit } from '@angular/core';
import { AuthService } from '@team-tasks/app/core/auth';
import { of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { GUEST_MENU_ITEMS, USER_MENU_ITEMS } from './app-menu';

@Component({
  selector: 'team-tasks-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  isAuthenticated$ = this.authService.isAuthenticated();

  guestMenuItems = GUEST_MENU_ITEMS;
  userMenuItems = USER_MENU_ITEMS;

  constructor(private readonly authService: AuthService) {}

  ngOnInit(): void {
    this.isAuthenticated$
      .pipe(switchMap(isAuthenticated => (isAuthenticated ? this.authService.getProfile() : of())))
      .subscribe();
  }
}
