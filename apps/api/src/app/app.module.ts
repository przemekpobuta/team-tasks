import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthModule } from '@team-tasks/api/auth';
import { InboxModule } from '@team-tasks/api/inbox';
import { ProjectsModule } from '@team-tasks/api/projects';
import { TeamsModule } from '@team-tasks/api/teams';
import { UsersModule } from '@team-tasks/api/users';
import * as mongoose from 'mongoose';

import { AppController } from './app.controller';
import { AppService } from './app.service';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost/teamtasks', {
      useFindAndModify: false
    }),
    UsersModule,
    AuthModule,
    InboxModule,
    ProjectsModule,
    TeamsModule
  ],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule {}

mongoose.set('debug', true);
