import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersModule } from '@team-tasks/api/users';

import { ProjectsController } from './projects.controller';
import { ProjectSchema } from './schemas/project.schema';
import { ProjectTasksService } from './services/project-tasks.service';
import { ProjectsService } from './services/projects.service';

const MONGOOSE_MODELS = [{ name: 'Project', schema: ProjectSchema }];

@Module({
  imports: [MongooseModule.forFeature(MONGOOSE_MODELS), UsersModule],
  providers: [ProjectsService, ProjectTasksService],
  controllers: [ProjectsController],
  exports: [ProjectTasksService]
})
export class ProjectsModule {}
