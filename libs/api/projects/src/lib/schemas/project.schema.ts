import * as mongoose from 'mongoose';

import { TaskSchema } from './task.schema';

export const ProjectSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  description: String,
  projectOwner: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  tasks: [TaskSchema],
  team: { type: mongoose.Schema.Types.ObjectId, ref: 'Team' }
});
