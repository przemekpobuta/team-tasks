import * as mongoose from 'mongoose';

export const TaskSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  completed: {
    type: Boolean,
    default: false
  },
  priority: {
    type: Boolean,
    default: false
  }
});
