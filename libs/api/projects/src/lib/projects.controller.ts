import { Body, Controller, Delete, Get, Logger, Param, Patch, Post, Put, Request, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ChangeTaskProjectDto,
  CreateProjectDto,
  CreateProjectTaskDto,
  Project,
  Task,
  UpdateProjectDto,
  UpdateProjectTaskDto,
} from '@team-tasks/shared/interfaces';

import { ProjectTasksService } from './services/project-tasks.service';
import { ProjectsService } from './services/projects.service';

@Controller('projects')
export class ProjectsController {
  constructor(private readonly projectsService: ProjectsService, private readonly projectTasksService: ProjectTasksService) {}

  private logger = new Logger(ProjectsController.name);

  @UseGuards(AuthGuard('jwt'))
  @Post()
  async createProject(@Body() createProjectDto: CreateProjectDto, @Request() { user: { userId } }): Promise<Project> {
    return this.projectsService.create(userId, createProjectDto);
  }

  @UseGuards(AuthGuard('jwt'))
  @Get()
  async getUserProjects(@Request() { user: { userId } }): Promise<Project[]> {
    return this.projectsService.getUserProjects(userId);
  }

  @UseGuards(AuthGuard('jwt'))
  @Put('/:id')
  async updateProject(
    @Param('id') projectId: string,
    @Body() updateProjectDto: UpdateProjectDto,
    @Request() { user: { userId } }
  ): Promise<Project> {
    return this.projectsService.update(userId, projectId, updateProjectDto);
  }

  @UseGuards(AuthGuard('jwt'))
  @Delete('/:id')
  async deleteProject(@Param('id') projectId: string, @Request() { user: { userId } }): Promise<Project> {
    return this.projectsService.delete(userId, projectId);
  }

  @UseGuards(AuthGuard('jwt'))
  @Post('/:projectId/tasks')
  async createProjectTask(
    @Request() { user: { userId } },
    @Param('projectId') projectId: string,
    @Body() createProjectTaskDto: CreateProjectTaskDto
  ): Promise<Task> {
    return this.projectTasksService.create(userId, projectId, createProjectTaskDto);
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('/:projectId/tasks')
  async getProjectTasks(@Param('projectId') projectId: string, @Request() { user: { userId } }): Promise<Task[]> {
    return this.projectTasksService.get(userId, projectId);
  }

  @UseGuards(AuthGuard('jwt'))
  @Put('/:projectId/tasks/:taskId')
  async updateProjectTask(
    @Request() { user: { userId } },
    @Param('projectId') projectId: string,
    @Param('taskId') taskId: string,
    @Body() updateProjectTaskDto: UpdateProjectTaskDto
  ): Promise<Task> {
    return this.projectTasksService.update(userId, projectId, taskId, updateProjectTaskDto);
  }

  @UseGuards(AuthGuard('jwt'))
  @Delete('/:projectId/tasks/:taskId')
  async deleteProjectTask(
    @Request() { user: { userId } },
    @Param('projectId') projectId: string,
    @Param('taskId') taskId: string
  ): Promise<Task> {
    return this.projectTasksService.delete(userId, projectId, taskId);
  }

  @UseGuards(AuthGuard('jwt'))
  @Patch('/:projectId/tasks/:taskId/changeProject')
  async changeTaskProject(
    @Request() { user: { userId } },
    @Param('projectId') projectId: string,
    @Param('taskId') taskId: string,
    @Body() changeTaskProjectDto: ChangeTaskProjectDto
  ): Promise<Task> {
    return this.projectTasksService.changeTaskProject(userId, projectId, taskId, changeTaskProjectDto);
  }
}
