import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import {
  ChangeTaskProjectDto,
  CreateProjectTaskDto,
  ProjectModel,
  Task,
  UpdateProjectTaskDto,
} from '@team-tasks/shared/interfaces';
import { Model } from 'mongoose';

@Injectable()
export class ProjectTasksService {
  constructor(@InjectModel('Project') private readonly projectModel: Model<ProjectModel>) {}

  private logger = new Logger(ProjectTasksService.name);

  async create(userId: string, projectId: string, task: CreateProjectTaskDto): Promise<Task> {
    try {
      const project = await this.getProject(userId, projectId);

      project.tasks.push(task as Task);
      const updatedProject = await project.save();

      if (!updatedProject) {
        throw new HttpException('Can not create task', HttpStatus.INTERNAL_SERVER_ERROR);
      }

      return updatedProject.tasks.find(addedTask => addedTask.name === task.name);
    } catch (err) {
      throw new HttpException(err.message, err.status);
    }
  }

  async get(userId: string, projectId: string): Promise<Task[]> {
    try {
      const project = await this.getProject(userId, projectId);

      return project.get('tasks');
    } catch (err) {
      throw new HttpException(err.message, err.status);
    }
  }

  async update(userId: string, projectId: string, taskId: string, taskUpdate: UpdateProjectTaskDto): Promise<Task> {
    this.logger.debug(taskUpdate);

    try {
      const project = await this.getProject(userId, projectId);

      const taskToUpdate = project.tasks.find(task => task._id.toString() === taskId);
      if (!taskToUpdate) {
        throw new HttpException('Task do not exist', HttpStatus.NOT_FOUND);
      }

      const updatedProject = await this.projectModel
        .findOneAndUpdate(
          {
            _id: projectId,
            'tasks._id': taskId
          },
          {
            $set: Object.keys(taskUpdate)
              .filter(k => k !== '_id')
              .reduce((acc, curr) => Object.assign(acc, { [`tasks.$.${curr}`]: taskUpdate[curr] }), {})
          },
          {
            new: true
          }
        )
        .exec();

      if (!updatedProject) {
        throw new HttpException('Can not update task', HttpStatus.INTERNAL_SERVER_ERROR);
      }

      return updatedProject.tasks.find(task => task._id.toString() === taskId);
    } catch (err) {
      throw new HttpException(err.message, err.status);
    }
  }

  async delete(userId: string, projectId: string, taskId: string): Promise<Task> {
    try {
      const project = await this.getProject(userId, projectId);

      const taskToRemove = project.tasks.find(task => task._id.toString() === taskId);

      if (!taskToRemove) {
        throw new HttpException('Task do not exist', HttpStatus.NOT_FOUND);
      }

      project.tasks = project.tasks.filter(task => task._id.toString() !== taskId);

      await project.save();

      return taskToRemove;
    } catch (err) {
      throw new HttpException(err.message, err.status);
    }
  }

  async changeTaskProject(
    userId: string,
    projectId: string,
    taskId: string,
    changeTaskProjectDto: ChangeTaskProjectDto
  ): Promise<Task> {
    this.logger.debug(`projectId: ${projectId}`);
    this.logger.debug(`targetProjectId: ${changeTaskProjectDto.targetProjectId}`);
    this.logger.debug(`tastId: ${taskId}`);
    try {
      if (projectId === changeTaskProjectDto.targetProjectId) {
        throw new HttpException('Project and target project are the same', HttpStatus.BAD_REQUEST);
      }

      const project = await this.getProject(userId, projectId);

      const task = project.tasks.find(oldTask => oldTask._id.toString() === taskId);
      this.logger.debug('foundTask: ' + task);

      if (!task) {
        throw new HttpException('Task not found', HttpStatus.NOT_FOUND);
      }

      const targetProject = await this.projectModel.findById(changeTaskProjectDto.targetProjectId).exec();

      if (!targetProject) {
        throw new HttpException('Target project not found', HttpStatus.NOT_FOUND);
      }

      targetProject.tasks.push(task);

      await targetProject.save();

      project.tasks = project.tasks.filter(item => item._id.toString() !== taskId);

      await project.save();

      return targetProject.tasks.find(item => item._id.toString() === taskId);
    } catch (err) {
      this.logger.error('changeTaskProject error ' + err);
      throw new HttpException(err.message, err.status);
    }
  }

  async addTaskToProject(userId: string, task: Task, projectId: string): Promise<Task> {
    try {
      const project = await this.getProject(userId, projectId);

      project.tasks.push(task);

      await project.save();

      return project.tasks.find(projectTask => projectTask._id === task._id);
    } catch (err) {
      this.logger.error('addTaskToProject error ' + err);
      throw new HttpException(err.message, err.status);
    }
  }

  private async getProject(userId: string, projectId: string): Promise<ProjectModel> {
    try {
      const project = await this.projectModel.findById(projectId).exec();

      if (!project) {
        throw new HttpException('Project not found', HttpStatus.NOT_FOUND);
      } else if (project.projectOwner.toString() !== userId) {
        this.logger.error(`E ProjectOwner: ${project.projectOwner} but user: ${userId}`);
      }

      return project;
    } catch (err) {
      this.logger.error('getProject error ' + err);
      throw new HttpException(err.message, err.status);
    }
  }
}
