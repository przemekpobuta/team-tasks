import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { UserProjectsService } from '@team-tasks/api/users';
import { CreateProjectDto, Project, ProjectModel, UpdateProjectDto } from '@team-tasks/shared/interfaces';
import { Model, Types } from 'mongoose';

@Injectable()
export class ProjectsService {
  constructor(
    private readonly userProjectsService: UserProjectsService,
    @InjectModel('Project') private readonly projectModel: Model<ProjectModel>
  ) {}

  private logger = new Logger(ProjectsService.name);

  async create(userId: string, createProjectDto: CreateProjectDto): Promise<Project> {
    const newProject: Partial<ProjectModel> = {
      ...createProjectDto,
      projectOwner: userId
    };
    const createdProject = new this.projectModel(newProject);

    try {
      await this.userProjectsService.assignProjectToUser(createdProject._id, userId);
      return await createdProject.save();
    } catch (err) {
      this.logger.error('create project error', err);
      throw new HttpException('Can not create project', 400);
    }
  }

  async getUserProjects(userId: string): Promise<Project[]> {
    const privateProjects = await this.projectModel.find({ projectOwner: Types.ObjectId(userId), team: null }).exec();
    let sharedProjects = await this.projectModel
      .find({ team: { $ne: null } })
      .populate({
        path: 'team',
        match: {
          $or: [{ owner: Types.ObjectId(userId) }, { members: Types.ObjectId(userId) }]
        }
      })
      .exec();

    sharedProjects = sharedProjects.filter(item => item.team !== null);

    return [...privateProjects, ...sharedProjects];
  }

  async update(userId: string, projectId: string, updateProjectDto: UpdateProjectDto): Promise<Project> {
    try {
      const project = await this.projectModel.findById(projectId).exec();
      this.logger.debug('project: ' + project);

      if (!project) {
        throw new HttpException('Not found project', HttpStatus.NOT_FOUND);
      }

      return await this.projectModel
        .findOneAndUpdate(
          { _id: projectId, projectOwner: userId },
          {
            $set: {
              ...updateProjectDto
            }
          },
          { new: true }
        )
        .exec();
    } catch (err) {
      this.logger.error(err);
      throw new HttpException(err.message, err.status);
    }
  }

  async delete(userId: string, projectId: string): Promise<Project> {
    try {
      const project = await this.projectModel.findById(projectId).exec();
      this.logger.debug('project: ' + project);

      if (!project) {
        throw new HttpException('Not found project', HttpStatus.NOT_FOUND);
      }

      return await this.projectModel.findOneAndDelete({ _id: projectId, projectOwner: userId });
    } catch (err) {
      this.logger.error(err);
      throw new HttpException(err.message, err.status);
    }
  }
}
