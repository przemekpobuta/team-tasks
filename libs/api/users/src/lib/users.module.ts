import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { UserSchema } from './schemas';
import { UserInboxService, UserProjectsService, UsersService } from './services';
import { UsersController } from './users.controller';

const MONGOOSE_MODELS = [{ name: 'User', schema: UserSchema }];

@Module({
  imports: [MongooseModule.forFeature(MONGOOSE_MODELS)],
  providers: [UsersService, UserInboxService, UserProjectsService],
  controllers: [UsersController],
  exports: [UsersService, UserInboxService, UserProjectsService]
})
export class UsersModule {}
