import * as bcrypt from 'bcryptjs';
import * as mongoose from 'mongoose';

import { InboxTaskSchema } from './inbox-task.schema';

export const UserSchema = new mongoose.Schema({
  name: String,
  username: {
    type: String,
    unique: true,
    required: true
  },
  email: {
    type: String,
    trim: true,
    lowercase: true,
    unique: true,
    required: 'Email address is required'
  },
  password: String,
  inboxTasks: [InboxTaskSchema],
  projects: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Project' }]
});

/**
 * Remove sensitive user data from response
 */
UserSchema.method('toJSON', function() {
  const user = this.toObject();

  delete user.password;
  delete user.updatedAt;

  return user;
});

/**
 * Hash password
 */
UserSchema.pre<any>('save', async function(next) {
  if (this.isModified('password')) {
    this.password = await bcrypt.hash(this.password, 8);
  }

  next();
});
