import { Injectable, Logger, HttpException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Task, UpdateInboxTaskDto, UserModel } from '@team-tasks/shared/interfaces';
import { Model } from 'mongoose';

@Injectable()
export class UserInboxService {
  constructor(@InjectModel('User') private readonly userModel: Model<UserModel>) {}

  private logger = new Logger(UserInboxService.name);

  async createInboxTask(userId: string, task: Partial<Task>): Promise<Task> {
    const user = await this.userModel.findById(userId);
    user.inboxTasks.push(task as any);
    const savedUser = await user.save();

    return savedUser.inboxTasks.find(addedTask => addedTask.name === task.name);
  }

  async getInboxTasks(userId: string): Promise<Task[]> {
    const user = await this.userModel.findById(userId);
    
    return user.get('inboxTasks');
  }
  
  async getInboxTask(userId: string, taskId: string): Promise<Task> {
    try {
      const user = await this.userModel.findById(userId);    
      return user.inboxTasks.find(task => task.id === taskId);
    } catch (err) {
      this.logger.error(err);
      throw new HttpException(err.message, err.status);
    }
  }

  async updateInboxTask(userId: string, taskId: string, taskUpdate: UpdateInboxTaskDto): Promise<Task> {
    this.logger.debug(taskUpdate);
    await this.userModel
      .findOneAndUpdate(
        {
          _id: userId,
          'inboxTasks._id': taskId
        },
        {
          $set: Object.keys(taskUpdate)
            .filter(k => k !== '_id')
            .reduce((acc, curr) => Object.assign(acc, { [`inboxTasks.$.${curr}`]: taskUpdate[curr] }), {})
        },
        {}
      )
      .exec();

    const user = await this.userModel.findById(userId);
    return user.inboxTasks.find(task => task.id === taskId);
  }

  async deleteInboxTask(userId: string, taskId: string) {
    const user = await this.userModel.findById(userId);

    const taskToDelete = user.inboxTasks.find(task => task.id === taskId);
    taskToDelete.remove();

    await user.save();

    return taskToDelete;
  }
}
