import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateUserDto, UpdateUserDto, User, UserModel } from '@team-tasks/shared/interfaces';
import { Model } from 'mongoose';

@Injectable()
export class UsersService {
  constructor(@InjectModel('User') private readonly userModel: Model<UserModel>) {}

  private logger = new Logger(UsersService.name);

  async create(createUserDto: CreateUserDto): Promise<User> {
    const createdUser = new this.userModel(createUserDto);
    return await createdUser.save();
  }

  async findAll(): Promise<User[]> {
    return await this.userModel.find().exec();
  }

  async findOne(username: string): Promise<User | undefined> {
    return await this.userModel.findOne({ username }).exec();
  }

  async updateUser(id: string, updateUserDto: UpdateUserDto): Promise<User> {
    return await this.userModel
      .findOneAndUpdate(
        { _id: id },
        {
          $set: {
            ...updateUserDto
          }
        },
        { new: true }
      )
      .exec();
  }

  async getUser(id: string): Promise<User> {
    try {
      const user = await this.userModel.findById(id).exec();

      if (!user) {
        throw new HttpException('User not found', HttpStatus.NOT_FOUND);
      }

      return user;
    } catch (err) {
      this.logger.error('getUserProfile error:' + err);
      throw new HttpException(err.message, err.status);
    }
  }

  async deleteUser(userId: string): Promise<User> {
    try {
      return await this.userModel.findByIdAndDelete(userId);
    } catch (err) {
      this.logger.error('deleteUser error:' + err);
      throw new HttpException(err.message, err.status);
    }
  }
}
