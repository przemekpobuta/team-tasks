import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User, UserModel } from '@team-tasks/shared/interfaces';
import { Model } from 'mongoose';

@Injectable()
export class UserProjectsService {
  constructor(@InjectModel('User') private readonly userModel: Model<UserModel>) {}

  private logger = new Logger(UserProjectsService.name);

  async assignProjectToUser(projectId: string, userId: string): Promise<User> {
    const user = await this.userModel.findById(userId);
    user.projects.push(projectId);

    return await user.save();
  }
}
