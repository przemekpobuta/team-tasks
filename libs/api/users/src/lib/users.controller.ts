import { Body, Controller, Delete, Get, Logger, Post, Put, Request, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { CreateUserDto, UpdateUserDto, User } from '@team-tasks/shared/interfaces';

import { UsersService } from './services/users.service';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  private logger = new Logger(UsersController.name);

  @Post()
  async create(@Body() createUserDto: CreateUserDto): Promise<User> {
    return await this.usersService.create(createUserDto);
  }

  @UseGuards(AuthGuard('jwt'))
  @Put()
  async updateOwnUser(@Request() { user: { userId } }, @Body() updateUserDto: UpdateUserDto): Promise<User> {
    return await this.usersService.updateUser(userId, updateUserDto);
  }

  @Get()
  async findAll(): Promise<User[]> {
    this.logger.debug('GET findAll');
    return await this.usersService.findAll();
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('me')
  async getProfile(@Request() { user: { userId } }): Promise<User> {
    return await this.usersService.getUser(userId);
  }

  @UseGuards(AuthGuard('jwt'))
  @Delete('me')
  async deleteOwnUser(@Request() { user: { userId } }): Promise<User> {
    return await this.usersService.deleteUser(userId);
  }
}
