import * as mongoose from 'mongoose';

import { TeamInvitationSchema } from './team-invitation.schema';

export const TeamSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  description: String,
  owner: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  members: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
  invitations: [TeamInvitationSchema]
});
