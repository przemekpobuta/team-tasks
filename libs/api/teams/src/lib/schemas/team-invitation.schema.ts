import * as mongoose from 'mongoose';

export const TeamInvitationSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true
  }
});
