import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersModule } from '@team-tasks/api/users';

import { TeamSchema } from './schemas/team.schema';
import { TeamsController } from './teams.controller';
import { TeamsService } from './teams.service';

const MONGOOSE_MODELS = [{ name: 'Team', schema: TeamSchema }];

@Module({
  imports: [MongooseModule.forFeature(MONGOOSE_MODELS), UsersModule],
  providers: [TeamsService],
  controllers: [TeamsController]
})
export class TeamsModule {}
