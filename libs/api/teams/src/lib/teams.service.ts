import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { UsersService } from '@team-tasks/api/users';
import {
  CreateTeamDto,
  InviteTeamMemberDto,
  Team,
  TeamInvitation,
  TeamInvitationAction,
  TeamModel,
  UpdateTeamDto,
  User,
} from '@team-tasks/shared/interfaces';
import { Model, Types } from 'mongoose';

@Injectable()
export class TeamsService {
  constructor(@InjectModel('Team') private readonly teamModel: Model<TeamModel>, private readonly usersService: UsersService) {}

  private logger = new Logger(TeamsService.name);

  async create(userId: string, createTeamDto: CreateTeamDto): Promise<Team> {
    const team: Partial<Team> = {
      ...createTeamDto,
      owner: userId
    };

    const teamModel = new this.teamModel(team);

    try {
      const createdTeam = await teamModel.save();
      return await createdTeam
        .populate('owner')
        .populate('members')
        .execPopulate();
    } catch (err) {
      this.logger.error('create team error', err);
      throw new HttpException("Can't create team", 400);
    }
  }

  async getTeamsByOwnerOrMember(userId: string): Promise<Team[]> {
    return await this.teamModel
      .find({ $or: [{ owner: Types.ObjectId(userId) }, { members: Types.ObjectId(userId) }] })
      .populate(['owner', 'members'])
      .exec();
  }

  async update(userId: string, teamId: string, updateTeamDto: UpdateTeamDto): Promise<Team> {
    try {
      const team = await this.teamModel.findById(teamId).exec();
      this.logger.debug('team: ' + team);

      if (!team) {
        throw new HttpException('Not found team', HttpStatus.NOT_FOUND);
      }

      const isUserOwnTeam = team && team.owner.toString() === userId;

      if (!isUserOwnTeam) {
        throw new HttpException('Forbidden', HttpStatus.FORBIDDEN);
      }

      const updatedTeam = await this.teamModel
        .findOneAndUpdate(
          { _id: teamId },
          {
            $set: {
              ...updateTeamDto
            }
          },
          {
            new: true
          }
        )
        .exec();

      return await updatedTeam
        .populate('owner')
        .populate('members')
        .execPopulate();
    } catch (err) {
      this.logger.error(err);
      throw new HttpException(err.message, err.status);
    }
  }

  async delete(userId: string, teamId: string): Promise<Team> {
    try {
      const team = await this.teamModel.findById(teamId).exec();
      this.logger.debug('team: ' + team);

      if (!team) {
        throw new HttpException('Not found team', HttpStatus.NOT_FOUND);
      }

      const isUserOwnTeam = team && team.owner.toString() === userId;

      if (!isUserOwnTeam) {
        throw new HttpException('Forbidden', HttpStatus.FORBIDDEN);
      }

      return await this.teamModel.findOneAndDelete({ _id: teamId });

      // TODO: change projects to private for owner
    } catch (err) {
      this.logger.error('delete team error:' + err);
      throw new HttpException(err.message, err.status);
    }
  }

  async inviteTeamMember(userId: string, teamId: string, inviteTeamMemberDto: InviteTeamMemberDto): Promise<TeamInvitation> {
    try {
      const team = await this.getTeam(teamId);

      if (team.owner.toString() !== userId) {
        this.logger.error(`TeamOwner: ${team.owner} but user: ${userId}`);
        throw new HttpException("User don't have permission to team", HttpStatus.FORBIDDEN);
      }

      const { email } = inviteTeamMemberDto;

      team.invitations.push({ email });

      await team.save();

      return team.invitations.find(item => item.email === email);
    } catch (err) {
      this.logger.error('inviteTeamMember error:' + err);
      throw new HttpException(err.message, err.status);
    }
  }

  async getTeamsWithInvitation(userId: string): Promise<Team[]> {
    try {
      const user = await this.usersService.getUser(userId);

      return await this.teamModel
        .find({ 'invitations.email': { $all: user.email } })
        .populate(['owner', 'members'])
        .exec();
    } catch (err) {
      this.logger.error('getTeamsWithInvitation error:' + err);
      throw new HttpException(err.message, err.status);
    }
  }

  async handleTeamMemberInvitationAction(userId: string, teamId: string, action: TeamInvitationAction): Promise<Team> {
    try {
      const user = await this.usersService.getUser(userId);
      const team = await this.getTeam(teamId);

      if (action === TeamInvitationAction.ACCEPT) {
        team.members.push(userId);
      }

      team.invitations = team.invitations.filter(item => item.email !== user.email);

      const changedTeam = await team.save();

      return await changedTeam
        .populate('owner')
        .populate('members')
        .execPopulate();
    } catch (err) {
      this.logger.error('handleTeamMemberInvitationAction error ' + err);
      throw new HttpException(err.message, err.status);
    }
  }

  async removeTeamMember(userId: string, teamId: string, userIdForRemove: string): Promise<User> {
    try {
      const team = await this.getTeam(teamId);

      if (team.owner.toString() !== userId) {
        throw new HttpException("User don't have permission to remove member", HttpStatus.FORBIDDEN);
      }

      const isUserMember = !!team.members.filter(item => item.toString() === userIdForRemove).length;

      this.logger.debug(`isUserMember ${isUserMember}`);

      if (!isUserMember) {
        throw new HttpException("Member don't exist", HttpStatus.BAD_REQUEST);
      }

      team.members = team.members.filter(item => item.toString() !== userIdForRemove);

      await team.save();

      return await this.usersService.getUser(userIdForRemove);
    } catch (err) {
      this.logger.error('removeTeamMember error:' + err);
      throw new HttpException(err.message, err.status);
    }
  }

  async leaveTeamMemberhip(userId: string, teamId: string): Promise<Team> {
    try {
      const team = await this.getTeam(teamId);

      const isUserMember = !!team.members.filter(item => item.toString() === userId).length;

      this.logger.debug(`isUserMember ${isUserMember}`);

      if (!isUserMember) {
        throw new HttpException("You're not team member", HttpStatus.FORBIDDEN);
      }

      team.members = team.members.filter(item => item.toString() !== userId);

      return await team.save();
    } catch (err) {
      this.logger.error('leaveTeamMemberhip error:' + err);
      throw new HttpException(err.message, err.status);
    }
  }

  private async getTeam(teamId: string): Promise<TeamModel> {
    try {
      const team = await this.teamModel.findById(teamId).exec();

      if (!team) {
        throw new HttpException('Team not found', HttpStatus.NOT_FOUND);
      }

      return team;
    } catch (err) {
      this.logger.error('getTeam error ' + err);
      throw new HttpException(err.message, err.status);
    }
  }
}
