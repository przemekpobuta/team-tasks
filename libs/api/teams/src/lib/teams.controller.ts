import { Body, Controller, Delete, Get, Param, Post, Put, Request, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  CreateTeamDto,
  InviteTeamMemberDto,
  Team,
  TeamInvitation,
  TeamInvitationAction,
  UpdateTeamDto,
  User,
} from '@team-tasks/shared/interfaces';

import { TeamsService } from './teams.service';

@Controller('teams')
export class TeamsController {
  constructor(private readonly teamsService: TeamsService) {}

  @UseGuards(AuthGuard('jwt'))
  @Post()
  async createTeam(@Body() createTeamDto: CreateTeamDto, @Request() { user: { userId } }): Promise<Team> {
    return await this.teamsService.create(userId, createTeamDto);
  }

  @UseGuards(AuthGuard('jwt'))
  @Get()
  async getTeams(@Request() { user: { userId } }): Promise<Team[]> {
    return await this.teamsService.getTeamsByOwnerOrMember(userId);
  }

  @UseGuards(AuthGuard('jwt'))
  @Put(':id')
  async updateTeam(
    @Param('id') teamId: string,
    @Body() updateTeamDto: UpdateTeamDto,
    @Request() { user: { userId } }
  ): Promise<Team> {
    return await this.teamsService.update(userId, teamId, updateTeamDto);
  }

  @UseGuards(AuthGuard('jwt'))
  @Delete(':id')
  async deleteTeam(@Param('id') teamId: string, @Request() { user: { userId } }): Promise<Team> {
    return await this.teamsService.delete(userId, teamId);
  }

  @UseGuards(AuthGuard('jwt'))
  @Post(':id/invite')
  async inviteTeamMember(
    @Param('id') teamId: string,
    @Body() inviteTeamMemberDto: InviteTeamMemberDto,
    @Request() { user: { userId } }
  ): Promise<TeamInvitation> {
    return await this.teamsService.inviteTeamMember(userId, teamId, inviteTeamMemberDto);
  }

  @UseGuards(AuthGuard('jwt'))
  @Post(':id/acceptInvitation')
  async acceptTeamMemberInvitation(@Param('id') teamId: string, @Request() { user: { userId } }): Promise<Team> {
    return await this.teamsService.handleTeamMemberInvitationAction(userId, teamId, TeamInvitationAction.ACCEPT);
  }

  @UseGuards(AuthGuard('jwt'))
  @Post(':id/ignoreInvitation')
  async ignoreTeamMemberInvitation(@Param('id') teamId: string, @Request() { user: { userId } }): Promise<Team> {
    return await this.teamsService.handleTeamMemberInvitationAction(userId, teamId, TeamInvitationAction.IGNORE);
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('invited')
  async getTeamsWithInvitation(@Request() { user: { userId } }): Promise<Team[]> {
    return await this.teamsService.getTeamsWithInvitation(userId);
  }

  @UseGuards(AuthGuard('jwt'))
  @Delete(':teamId/member/:userIdForRemove')
  async removeTeamMember(
    @Param('teamId') teamId: string,
    @Param('userIdForRemove') userIdForRemove: string,
    @Request() { user: { userId } }
  ): Promise<User> {
    return await this.teamsService.removeTeamMember(userId, teamId, userIdForRemove);
  }

  @UseGuards(AuthGuard('jwt'))
  @Delete(':id/member')
  async leaveTeamMemberhip(@Param('id') teamId: string, @Request() { user: { userId } }): Promise<Team> {
    return await this.teamsService.leaveTeamMemberhip(userId, teamId);
  }
}
