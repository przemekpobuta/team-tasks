import { Module } from '@nestjs/common';
import { ProjectsModule } from '@team-tasks/api/projects';
import { UsersModule } from '@team-tasks/api/users';

import { InboxController } from './inbox.controller';
import { InboxService } from './inbox.service';

@Module({
  imports: [UsersModule, ProjectsModule],
  providers: [InboxService],
  controllers: [InboxController],
  exports: [InboxService]
})
export class InboxModule {}
