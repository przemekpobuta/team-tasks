import { Body, Controller, Delete, Get, Param, Patch, Post, Put, Request, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { CreateInboxTaskDto, SetTaskProjectDto, Task, UpdateInboxTaskDto } from '@team-tasks/shared/interfaces';

import { InboxService } from './inbox.service';

@Controller('inbox')
export class InboxController {
  constructor(private readonly inboxService: InboxService) {}

  @UseGuards(AuthGuard('jwt'))
  @Get()
  getTasks(@Request() { user }): Promise<Task[]> {
    return this.inboxService.getTasks(user.userId);
  }

  @UseGuards(AuthGuard('jwt'))
  @Post()
  createTask(@Body() createInboxTaskDto: CreateInboxTaskDto, @Request() { user }): Promise<Task> {
    return this.inboxService.createTask(user.userId, createInboxTaskDto);
  }

  @UseGuards(AuthGuard('jwt'))
  @Put('/:id')
  updateTask(@Param('id') taskId: string, @Body() updateInboxTaskDto: UpdateInboxTaskDto, @Request() { user }): Promise<Task> {
    return this.inboxService.updateTask(user.userId, taskId, updateInboxTaskDto);
  }

  @UseGuards(AuthGuard('jwt'))
  @Delete('/:id')
  deleteTask(@Param('id') taskId: string, @Request() { user }) {
    return this.inboxService.deleteTask(user.userId, taskId);
  }

  @UseGuards(AuthGuard('jwt'))
  @Patch('/:id/setProject')
  setTaskProject(@Param('id') taskId: string, @Body() setTaskProjectDto: SetTaskProjectDto, @Request() { user }): Promise<Task> {
    return this.inboxService.setTaskProject(user.userId, taskId, setTaskProjectDto);
  }
}
