import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { ProjectTasksService } from '@team-tasks/api/projects';
import { UserInboxService } from '@team-tasks/api/users';
import { CreateInboxTaskDto, SetTaskProjectDto, Task, UpdateInboxTaskDto } from '@team-tasks/shared/interfaces';

@Injectable()
export class InboxService {
  constructor(private readonly userInboxService: UserInboxService, private readonly projectTaskService: ProjectTasksService) {}

  private logger = new Logger(InboxService.name);

  async getTasks(userId: string): Promise<Task[]> {
    return await this.userInboxService.getInboxTasks(userId);
  }

  async createTask(userId: string, createInboxTaskDto: CreateInboxTaskDto): Promise<Task> {
    const newTask: Partial<Task> = {
      ...createInboxTaskDto,
      completed: false
    };
    return await this.userInboxService.createInboxTask(userId, newTask);
  }

  async updateTask(userId: string, taskId: string, updateInboxTaskDto: UpdateInboxTaskDto): Promise<Task> {
    return await this.userInboxService.updateInboxTask(userId, taskId, updateInboxTaskDto);
  }

  async deleteTask(userId: string, taskId: string) {
    return await this.userInboxService.deleteInboxTask(userId, taskId);
  }

  async setTaskProject(userId: string, taskId: string, setTaskProjectDto: SetTaskProjectDto): Promise<Task> {
    this.logger.debug(`taskId: ${taskId}`);
    this.logger.debug('target project: ' + setTaskProjectDto);

    try {
      const inboxTask = await this.userInboxService.getInboxTask(userId, taskId);

      if (!inboxTask) {
        throw new HttpException('Task not found', HttpStatus.NOT_FOUND);
      }

      const { projectId } = setTaskProjectDto;

      const projectTask = await this.projectTaskService.addTaskToProject(userId, inboxTask, projectId);

      if (projectTask) {
        await this.userInboxService.deleteInboxTask(userId, taskId);
      }

      return projectTask;
    } catch (err) {
      this.logger.error(err);
      throw new HttpException(err.message, err.status);
    }
  }
}
