import { Injectable, Logger } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '@team-tasks/api/users';
import { User } from '@team-tasks/shared/interfaces';
import * as bcrypt from 'bcryptjs';

@Injectable()
export class AuthService {
  constructor(private readonly userService: UsersService, private readonly jwtService: JwtService) {}

  private logger = new Logger(AuthService.name);

  async validateUser(username: string, password: string): Promise<User> | null {
    const user = await this.userService.findOne(username);
    const passwordMatch = user ? await bcrypt.compare(password, user.password) : false;

    if (user && passwordMatch) {
      return user;
    }
    return null;
  }

  async login(user: any) {
    this.logger.debug('user: ' + user);
    const payload = { username: user.username, sub: user._id };
    this.logger.debug('payload ' + JSON.stringify(payload));
    const token = this.jwtService.sign(payload);

    return { token };
  }
}
