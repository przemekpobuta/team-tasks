export enum TeamInvitationAction {
  ACCEPT = 'Accept',
  IGNORE = 'Ignore'
}
