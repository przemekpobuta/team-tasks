import { Document } from 'mongoose';

import { Team } from './team.interface';

export interface TeamModel extends Team, Document {}
