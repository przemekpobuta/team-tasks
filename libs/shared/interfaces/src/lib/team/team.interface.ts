import { Base } from '../base.interface';
import { User } from '../users/user.interface';
import { TeamInvitation } from './team-invitation.interface';

export interface Team extends Base {
  name: string;
  description: string;
  owner: string | User;
  members: (string | User)[];
  invitations: TeamInvitation[];
}
