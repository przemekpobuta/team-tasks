export interface CreateTeamDto {
  readonly name: string;
  readonly description: string;
}
