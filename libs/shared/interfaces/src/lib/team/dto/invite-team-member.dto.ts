export interface InviteTeamMemberDto {
  readonly email: string;
}
