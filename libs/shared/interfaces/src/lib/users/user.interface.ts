import { Base } from '../base.interface';
import { Task } from '../tasks/task.interface';

export interface User extends Base {
  name: string;
  username: string;
  password: string;
  email: string;
  inboxTasks: Task[];
  projects: string[];
}
