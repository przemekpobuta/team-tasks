import { Document } from 'mongoose';

import { TaskModel } from '../tasks/task.model';
import { User } from './user.interface';

export interface UserModel extends User, Document {
  inboxTasks: TaskModel[];
}
