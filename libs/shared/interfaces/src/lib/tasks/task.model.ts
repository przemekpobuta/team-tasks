import * as mongoose from 'mongoose';

import { Task } from './task.interface';

export interface TaskModel extends Task, mongoose.Types.Subdocument {}
