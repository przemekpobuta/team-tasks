import { Base } from '../base.interface';

export interface Task extends Base {
  name: string;
  completed: boolean;
  priority: boolean;
}
