export interface UpdateInboxTaskDto {
  readonly name: string;
  readonly priority: boolean;
  readonly completed: boolean;
}
