export interface UpdateProjectTaskDto {
  readonly name: string;
  readonly priority: boolean;
  readonly completed: boolean;
}
