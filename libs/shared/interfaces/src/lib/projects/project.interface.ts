import { Base } from '../base.interface';
import { Task } from '../tasks/task.interface';
import { Team } from '../team/team.interface';
import { User } from '../users/user.interface';

export interface Project extends Base {
  name: string;
  description: string;
  projectOwner: string | User;
  tasks: Task[];
  team: string | Team;
}
