import { Document } from 'mongoose';

import { Project } from './project.interface';

export interface ProjectModel extends Project, Document {}
