export interface CreateProjectDto {
  readonly name: string;
  readonly description: string;
}
