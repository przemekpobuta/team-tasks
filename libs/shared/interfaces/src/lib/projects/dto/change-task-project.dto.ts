export interface ChangeTaskProjectDto {
  readonly targetProjectId: string;
}
