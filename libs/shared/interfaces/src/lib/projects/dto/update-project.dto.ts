export interface UpdateProjectDto {
  readonly name: string;
  readonly description: string;
  readonly team: string;
}
