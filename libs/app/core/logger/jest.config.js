module.exports = {
  name: 'app-core-logger',
  preset: '../../../../jest.config.js',
  coverageDirectory: '../../../../coverage/libs/app/core/logger',
  snapshotSerializers: ['jest-preset-angular/AngularSnapshotSerializer.js', 'jest-preset-angular/HTMLCommentSerializer.js']
};
