import { ModuleWithProviders, NgModule } from '@angular/core';
import { LoggerModule as NgxLoggerModule, NgxLoggerLevel } from 'ngx-logger';

export const PROVIDERS = [
  NgxLoggerModule.forRoot({
    level: NgxLoggerLevel.DEBUG,
    enableSourceMaps: false
  }).providers
];

@NgModule()
export class AppCoreLoggerModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: AppCoreLoggerModule,
      providers: PROVIDERS
    };
  }
}
