import { async, TestBed } from '@angular/core/testing';
import { AppCoreLoggerModule } from './app-core-logger.module';

describe('AppCoreLoggerModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppCoreLoggerModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(AppCoreLoggerModule).toBeDefined();
  });
});
