import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CreateUserDto, User } from '@team-tasks/shared/interfaces';
import { environment } from 'apps/app/src/environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { AuthStore } from './store/auth.store';

@Injectable({ providedIn: 'root' })
export class AuthService {
  constructor(private readonly router: Router, private readonly httpClient: HttpClient, private readonly authStore: AuthStore) {}
  private isAuthenticatedSubject = new BehaviorSubject<boolean>(this.hasToken());

  login(username: string, password: string): Observable<any> {
    return this.httpClient
      .post(`${environment.apiUrl}/auth/login`, {
        username,
        password
      })
      .pipe(tap(authResult => this.setSession(authResult.token)));
  }

  register(createUserDto: CreateUserDto): Observable<User> {
    return this.httpClient.post<User>(`${environment.apiUrl}/users`, createUserDto);
  }

  logout(): void {
    localStorage.removeItem('auth_token');
    this.isAuthenticatedSubject.next(false);
    this.router.navigateByUrl('/auth/login');
  }

  isAuthenticated(): Observable<boolean> {
    return this.isAuthenticatedSubject.asObservable();
  }

  getAuthToken(): string {
    return localStorage.getItem('auth_token');
  }

  getProfile(): Observable<User> {
    return this.httpClient.get<User>(`${environment.apiUrl}/users/me`).pipe(
      tap(profile =>
        this.authStore.update(() => ({
          profile
        }))
      )
    );
  }

  private setSession(token: string) {
    localStorage.setItem('auth_token', token);
    this.isAuthenticatedSubject.next(true);
  }

  private hasToken(): boolean {
    return !!localStorage.getItem('auth_token');
  }
}
