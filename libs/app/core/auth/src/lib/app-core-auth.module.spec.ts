import { async, TestBed } from '@angular/core/testing';

import { AppCoreAuthModule } from './app-core-auth.module';

describe('AppCoreAuthModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppCoreAuthModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(AppCoreAuthModule).toBeDefined();
  });
});
