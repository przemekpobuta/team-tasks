import { Injectable } from '@angular/core';
import { Query } from '@datorama/akita';
import { User } from '@team-tasks/shared/interfaces';
import { Observable } from 'rxjs';

import { AuthState, AuthStore } from './auth.store';

@Injectable({ providedIn: 'root' })
export class AuthQuery extends Query<AuthState> {
  constructor(protected store: AuthStore) {
    super(store);
  }

  selectProfile(): Observable<User> {
    return this.select(state => state.profile);
  }

  selectUserId(): Observable<string> {
    return this.select(state => state.profile._id);
  }
}
