module.exports = {
  name: 'app-core-auth',
  preset: '../../../../jest.config.js',
  coverageDirectory: '../../../../coverage/libs/app/core/auth',
  snapshotSerializers: ['jest-preset-angular/AngularSnapshotSerializer.js', 'jest-preset-angular/HTMLCommentSerializer.js']
};
