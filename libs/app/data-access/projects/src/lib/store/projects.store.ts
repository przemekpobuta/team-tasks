import { Injectable } from '@angular/core';
import { ActiveState, EntityState, EntityStore, EntityUIStore, StoreConfig } from '@datorama/akita';
import { Project } from '@team-tasks/shared/interfaces';

export interface ProjectUI {
  active: string;
}

export interface ProjectsState extends EntityState<Project>, ActiveState {}
export interface ProjectsUIState extends EntityState<ProjectUI> {}

const initialState = {
  active: null
};

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'projects', idKey: '_id' })
export class ProjectsStore extends EntityStore<ProjectsState> {
  ui: EntityUIStore<ProjectsUIState>;

  constructor() {
    super(initialState);
    this.createUIStore().setInitialEntityState({
      active: null
    });
  }
}
