import { Injectable } from '@angular/core';
import { EntityUIQuery, QueryEntity } from '@datorama/akita';
import { Logger } from '@team-tasks/app/core/logger';
import { Task } from '@team-tasks/shared/interfaces';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { AppDataAccessProjectsModule } from '../app-data-access-projects.module';
import { ProjectsState, ProjectsStore, ProjectsUIState } from './projects.store';

@Injectable({ providedIn: AppDataAccessProjectsModule })
export class ProjectsQuery extends QueryEntity<ProjectsState> {
  ui: EntityUIQuery<ProjectsUIState>;

  constructor(protected store: ProjectsStore, private readonly logger: Logger) {
    super(store);
    this.createUIQuery();
  }

  hasProjectActiveTask(): Observable<boolean> {
    return this.selectActiveId().pipe(
      switchMap(activeProjectId => {
        return this.ui.selectEntity(activeProjectId, entity => entity.active).pipe(map(active => !!active));
      })
    );
  }

  selectActiveProjectTasks(): Observable<Task[]> {
    return this.selectActive(project => project.tasks);
  }

  getActiveProjectId(): string {
    return this.getActiveId() ? this.getActiveId().toString() : null;
  }

  getProjectActiveTaskId(): string {
    return this.ui.getEntity(this.getActiveId()).active;
  }

  selectProjectActiveTask(): Observable<Task> {
    return this.hasProjectActiveTask().pipe(
      switchMap(() => {
        return this.selectActive(entity => entity.tasks.find(task => task._id === this.getProjectActiveTaskId()));
      })
    );
  }

  getProjectTasksQty(projectId: string): number {
    return this.getEntity(projectId).tasks.length;
  }
}
