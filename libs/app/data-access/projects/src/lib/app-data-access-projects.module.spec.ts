import { async, TestBed } from '@angular/core/testing';
import { AppDataAccessProjectsModule } from './app-data-access-projects.module';

describe('AppDataAccessProjectsModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppDataAccessProjectsModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(AppDataAccessProjectsModule).toBeDefined();
  });
});
