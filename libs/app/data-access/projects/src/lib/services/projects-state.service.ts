import { Injectable } from '@angular/core';
import { arrayAdd, arrayRemove, arrayUpdate } from '@datorama/akita';
import { Logger } from '@team-tasks/app/core/logger';
import {
  CreateProjectDto,
  CreateProjectTaskDto,
  Project,
  Task,
  UpdateProjectDto,
  UpdateProjectTaskDto,
} from '@team-tasks/shared/interfaces';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { AppDataAccessProjectsModule } from '../app-data-access-projects.module';
import { ProjectsStore } from '../store/projects.store';
import { ProjectTasksService } from './project-tasks.service';
import { ProjectsService } from './projects.service';

@Injectable({ providedIn: AppDataAccessProjectsModule })
export class ProjectsStateService {
  constructor(
    private readonly logger: Logger,
    private readonly projectsStore: ProjectsStore,
    private readonly projectsService: ProjectsService,
    private readonly projectTasksService: ProjectTasksService
  ) {}

  fetchProjects(): void {
    this.projectsService
      .get()
      .pipe(
        tap(
          projects => {
            this.logger.debug('fetchProjects', projects);
            this.projectsStore.set(projects);
          },
          error => {
            this.logger.error('fetchProjects error', error);
          }
        )
      )
      .subscribe();
  }

  createProject(project: CreateProjectDto): Observable<Project> {
    return this.projectsService.create(project).pipe(
      tap(
        (createdProject: Project) => {
          this.logger.debug('createProject', createdProject);
          this.projectsStore.add(createdProject);
        },
        error => {
          this.logger.error('createProject error', error);
        }
      )
    );
  }

  updateProject(projectId: string, update: Partial<UpdateProjectDto>): Observable<Project> {
    return this.projectsService.update(projectId, update).pipe(
      tap(
        (updatedProject: Project) => {
          this.logger.debug('updateProject:', updatedProject);
          this.projectsStore.replace(updatedProject._id, updatedProject);
        },
        error => {
          this.logger.error('updateProject error', error);
        }
      )
    );
  }

  deleteProject(projectId: string): Observable<Project> {
    return this.projectsService.delete(projectId).pipe(
      tap(
        (deletedProject: Project) => {
          this.logger.debug('deleteProject:', deletedProject);
          this.projectsStore.remove(deletedProject._id);
        },
        error => {
          this.logger.error('deleteProject error', error);
        }
      )
    );
  }

  setSelectedProject(projectId: string): void {
    this.projectsStore.setActive(projectId);
    this.resetProjectActiveTask(projectId);
  }

  resetSelectedProject(): void {
    this.projectsStore.update({
      active: null
    });
  }

  createProjectTask(projectId: string, createProjectTaskDto: CreateProjectTaskDto): Observable<Task> {
    return this.projectTasksService.create(projectId, createProjectTaskDto).pipe(
      tap(
        (createdTask: Task) => {
          this.logger.debug('createProjectTask', createdTask);
          this.projectsStore.update(projectId, project => {
            return {
              tasks: arrayAdd(project.tasks, createdTask)
            };
          });
        },
        error => {
          this.logger.error('createProject error', error);
        }
      )
    );
  }

  updateProjectTaskState(projectId: string, taskId: string, newState: boolean): Observable<Task> {
    const update: Partial<UpdateProjectTaskDto> = { completed: newState };
    return this.updateProjectTask(projectId, taskId, update);
  }

  updateProjectTaskPriority(projectId: string, taskId: string, newState: boolean): Observable<Task> {
    const update: Partial<UpdateProjectTaskDto> = { priority: newState };
    return this.updateProjectTask(projectId, taskId, update);
  }

  updateProjectTask(projectId: string, taskId: string, update: Partial<UpdateProjectTaskDto>): Observable<Task> {
    return this.projectTasksService.update(projectId, taskId, update).pipe(
      tap(
        (updatedTask: Task) => {
          this.logger.debug('updateProjectTask', updatedTask);
          this.projectsStore.update(projectId, project => {
            return {
              tasks: arrayUpdate(project.tasks, task => task._id === taskId, updatedTask)
            };
          });
        },
        error => {
          this.logger.error('updateProjectTask error', error);
        }
      )
    );
  }

  changeTaskProject(projectId: string, targetProjectId: string, taskId: string): Observable<Task> {
    return this.projectTasksService.changeProject(projectId, targetProjectId, taskId).pipe(
      tap(
        (updatedTask: Task) => {
          this.logger.debug('changeTaskProject', updatedTask);
          this.projectsStore.update(projectId, project => {
            return {
              tasks: arrayRemove(project.tasks, task => task._id === updatedTask._id)
            };
          });
          this.projectsStore.update(targetProjectId, project => {
            return {
              tasks: arrayAdd(project.tasks, { ...updatedTask })
            };
          });
          this.setSelectedProject(targetProjectId);
          this.setProjectActiveTask(targetProjectId, updatedTask._id);
        },
        error => {
          this.logger.error('changeTaskProject error', error);
        }
      )
    );
  }

  deleteProjectTask(projectId: string, taskId: string): Observable<Task> {
    return this.projectTasksService.delete(projectId, taskId).pipe(
      tap(
        (deletedTask: Task) => {
          this.logger.debug('deleteProjectTask:', deletedTask);
          this.projectsStore.update(projectId, project => ({
            tasks: arrayRemove(project.tasks, task => task._id === taskId)
          }));
          this.resetProjectActiveTask(projectId);
        },
        error => {
          this.logger.error('deleteProjectTask error', error);
        }
      )
    );
  }

  setProjectActiveTask(projectId: string, taskId: string): void {
    this.logger.debug(`setActiveProjectTask projectId: ${projectId}, taskId: ${taskId}`);
    this.projectsStore.ui.update(projectId, { active: taskId });
  }

  resetProjectActiveTask(projectId: string): void {
    this.logger.debug(`resetProjectActiveTask projectId: ${projectId}`);
    this.projectsStore.ui.update(projectId, { active: null });
  }

  toggleActiveProjectTask(projectId: string, taskId: string, state: boolean): void {
    state ? this.resetProjectActiveTask(projectId) : this.setProjectActiveTask(projectId, taskId);
  }

  assignProjectToTeam(projectId: string, teamId: string): Observable<Project> {
    const update: Partial<UpdateProjectDto> = { team: teamId };
    return this.updateProject(projectId, update);
  }

  makeProjectPrivate(projectId: string): Observable<Project> {
    const update: Partial<UpdateProjectDto> = { team: null };
    return this.updateProject(projectId, update);
  }
}
