import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CreateProjectDto, Project, UpdateProjectDto } from '@team-tasks/shared/interfaces';
import { environment } from 'apps/app/src/environments/environment';
import { Observable } from 'rxjs';

import { AppDataAccessProjectsModule } from '../app-data-access-projects.module';

@Injectable({
  providedIn: AppDataAccessProjectsModule
})
export class ProjectsService {
  constructor(private readonly httpClient: HttpClient) {}

  get(): Observable<Project[]> {
    return this.httpClient.get<Project[]>(`${environment.apiUrl}/projects`);
  }

  create(project: CreateProjectDto): Observable<Project> {
    return this.httpClient.post<Project>(`${environment.apiUrl}/projects`, project);
  }

  update(projectId: string, update: Partial<UpdateProjectDto>): Observable<Project> {
    return this.httpClient.put<Project>(`${environment.apiUrl}/projects/${projectId}`, update);
  }

  delete(projectId: string): Observable<Project> {
    return this.httpClient.delete<Project>(`${environment.apiUrl}/projects/${projectId}`);
  }
}
