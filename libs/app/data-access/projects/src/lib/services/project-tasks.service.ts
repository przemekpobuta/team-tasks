import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ChangeTaskProjectDto, CreateProjectTaskDto, Task, UpdateProjectTaskDto } from '@team-tasks/shared/interfaces';
import { environment } from 'apps/app/src/environments/environment';
import { Observable } from 'rxjs';

import { AppDataAccessProjectsModule } from '../app-data-access-projects.module';

@Injectable({
  providedIn: AppDataAccessProjectsModule
})
export class ProjectTasksService {
  constructor(private readonly httpClient: HttpClient) {}

  get(projectId: string): Observable<Task[]> {
    return this.httpClient.get<Task[]>(`${this.getProjectTasksApiUrl(projectId)}`);
  }

  create(projectId: string, task: CreateProjectTaskDto): Observable<Task> {
    return this.httpClient.post<Task>(`${this.getProjectTasksApiUrl(projectId)}`, task);
  }

  update(projectId: string, taskId: string, task: Partial<UpdateProjectTaskDto>): Observable<Task> {
    return this.httpClient.put<Task>(`${this.getProjectTasksApiUrl(projectId)}/${taskId}`, task);
  }

  delete(projectId: string, taskId: string): Observable<Task> {
    return this.httpClient.delete<Task>(`${this.getProjectTasksApiUrl(projectId)}/${taskId}`);
  }

  changeProject(projectId: string, targetProjectId: string, taskId: string): Observable<Task> {
    const changeProject: ChangeTaskProjectDto = {
      targetProjectId
    };
    return this.httpClient.patch<Task>(`${this.getProjectTasksApiUrl(projectId)}/${taskId}/changeProject`, changeProject);
  }

  private getProjectTasksApiUrl(projectId: string): string {
    return `${environment.apiUrl}/projects/${projectId}/tasks`;
  }
}
