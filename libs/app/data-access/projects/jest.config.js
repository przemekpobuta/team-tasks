module.exports = {
  name: 'app-data-access-projects',
  preset: '../../../../jest.config.js',
  coverageDirectory: '../../../../coverage/libs/app/data-access/projects',
  snapshotSerializers: ['jest-preset-angular/AngularSnapshotSerializer.js', 'jest-preset-angular/HTMLCommentSerializer.js']
};
