module.exports = {
  name: 'app-data-access-team',
  preset: '../../../../jest.config.js',
  coverageDirectory: '../../../../coverage/libs/app/data-access/team',
  snapshotSerializers: ['jest-preset-angular/AngularSnapshotSerializer.js', 'jest-preset-angular/HTMLCommentSerializer.js']
};
