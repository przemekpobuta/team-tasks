import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  CreateTeamDto,
  InviteTeamMemberDto,
  Team,
  TeamInvitation,
  UpdateTeamDto,
  User,
} from '@team-tasks/shared/interfaces';
import { environment } from 'apps/app/src/environments/environment';
import { Observable } from 'rxjs';

import { AppDataAccessTeamsModule } from '../app-data-access-teams.module';

@Injectable({
  providedIn: AppDataAccessTeamsModule
})
export class TeamsService {
  constructor(private readonly httpClient: HttpClient) {}

  get(): Observable<Team[]> {
    return this.httpClient.get<Team[]>(`${environment.apiUrl}/teams`);
  }

  create(team: CreateTeamDto): Observable<Team> {
    return this.httpClient.post<Team>(`${environment.apiUrl}/teams`, team);
  }

  update(teamId: string, update: Partial<UpdateTeamDto>): Observable<Team> {
    return this.httpClient.put<Team>(`${environment.apiUrl}/teams/${teamId}`, update);
  }

  delete(teamId: string): Observable<Team> {
    return this.httpClient.delete<Team>(`${environment.apiUrl}/teams/${teamId}`);
  }

  inviteTeamMember(teamId: string, inviteTeamMemberDto: InviteTeamMemberDto): Observable<TeamInvitation> {
    return this.httpClient.post<TeamInvitation>(`${environment.apiUrl}/teams/${teamId}/invite`, inviteTeamMemberDto);
  }

  getTeamInvitations(): Observable<Team[]> {
    return this.httpClient.get<Team[]>(`${environment.apiUrl}/teams/invited`);
  }

  acceptTeamInvitation(teamId: string): Observable<Team> {
    return this.httpClient.post<Team>(`${environment.apiUrl}/teams/${teamId}/acceptInvitation`, {});
  }

  ignoreTeamInvitation(teamId: string): Observable<Team> {
    return this.httpClient.post<Team>(`${environment.apiUrl}/teams/${teamId}/ignoreInvitation`, {});
  }

  leaveTeamMembership(teamId: string): Observable<Team> {
    return this.httpClient.delete<Team>(`${environment.apiUrl}/teams/${teamId}/member`);
  }

  deleteTeamMember(teamId: string, userId: string): Observable<User> {
    return this.httpClient.delete<User>(`${environment.apiUrl}/teams/${teamId}/member/${userId}`);
  }
}
