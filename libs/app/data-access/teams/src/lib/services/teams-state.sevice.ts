import { Injectable } from '@angular/core';
import { arrayRemove } from '@datorama/akita';
import { Logger } from '@team-tasks/app/core/logger';
import {
  CreateTeamDto,
  InviteTeamMemberDto,
  Team,
  TeamInvitation,
  UpdateTeamDto,
  User,
} from '@team-tasks/shared/interfaces';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { AppDataAccessTeamsModule } from '../app-data-access-teams.module';
import { TeamInvitationsStore } from '../store/team-invitations/team-invitations.store';
import { TeamsStore } from '../store/teams.store';
import { TeamsService } from './teams.service';

@Injectable({
  providedIn: AppDataAccessTeamsModule
})
export class TeamsStateService {
  constructor(
    private readonly logger: Logger,
    private readonly teamsStore: TeamsStore,
    private readonly teamsService: TeamsService,
    private readonly teamInvitationsStore: TeamInvitationsStore
  ) {}

  fetchTeams(): void {
    this.teamsService
      .get()
      .pipe(
        tap(
          teams => {
            this.logger.debug('fetchTeams', teams);
            this.teamsStore.set(teams);
          },
          error => {
            this.logger.error('fetchTeams error', error);
          }
        )
      )
      .subscribe();
  }

  createTeam(team: CreateTeamDto): Observable<Team> {
    return this.teamsService.create(team).pipe(
      tap(
        (createdTeam: Team) => {
          this.logger.debug('createTeam', createdTeam);
          this.teamsStore.add(createdTeam);
        },
        error => {
          this.logger.error('createTeam error', error);
        }
      )
    );
  }

  updateTeam(teamId: string, update: Partial<UpdateTeamDto>): Observable<Team> {
    return this.teamsService.update(teamId, update).pipe(
      tap(
        (updatedTeam: Team) => {
          this.logger.debug('updateTeam:', updatedTeam);
          this.teamsStore.replace(updatedTeam._id, updatedTeam);
        },
        error => {
          this.logger.error('updateTeam error', error);
        }
      )
    );
  }

  deleteTeam(teamId: string): Observable<Team> {
    return this.teamsService.delete(teamId).pipe(
      tap(
        (deletedTeam: Team) => {
          this.logger.debug('deleteTeam:', deletedTeam);
          this.teamsStore.remove(deletedTeam._id);
        },
        error => {
          this.logger.error('deleteTeam error', error);
        }
      )
    );
  }

  setTeamActive(teamId: string): void {
    this.teamsStore.setActive(teamId);
  }

  inviteUserToTeam(teamId: string, email: string): Observable<TeamInvitation> {
    const inviteTeamMemberDto: InviteTeamMemberDto = { email };
    return this.teamsService.inviteTeamMember(teamId, inviteTeamMemberDto).pipe(
      tap(
        value => {
          this.logger.debug('inviteUserToTeam', value);
        },
        error => {
          this.logger.error('inviteUserToTeam error', error);
        }
      )
    );
  }

  fetchTeamInvitations(): void {
    this.teamsService.getTeamInvitations().subscribe(
      teamInvitations => {
        this.logger.debug('getTeamInvitations', teamInvitations);
        this.teamInvitationsStore.set(teamInvitations);
      },
      error => {
        this.logger.error('getTeamInvitations error', error);
      }
    );
  }

  acceptTeamInvitation(teamId: string): Observable<Team> {
    return this.teamsService.acceptTeamInvitation(teamId).pipe(
      tap(
        team => {
          this.logger.debug('acceptTeamInvitation', team);
          this.teamsStore.add(team);
          this.teamInvitationsStore.remove(teamId);
        },
        error => {
          this.logger.error('acceptTeamInvitation error', error);
        }
      )
    );
  }

  ignoreTeamInvitation(teamId: string): Observable<Team> {
    return this.teamsService.ignoreTeamInvitation(teamId).pipe(
      tap(
        team => {
          this.logger.debug('ignoreTeamInvitation', team);
          this.teamInvitationsStore.remove(teamId);
        },
        error => {
          this.logger.error('ignoreTeamInvitation', error);
        }
      )
    );
  }

  leaveTeamMembership(teamId: string): Observable<Team> {
    return this.teamsService.leaveTeamMembership(teamId).pipe(
      tap(
        team => {
          this.logger.debug('leaveTeamMembership', team);
          this.teamsStore.remove(teamId);
        },
        error => {
          this.logger.error('leaveTeamMembership', error);
        }
      )
    );
  }

  deleteTeamMember(teamId: string, userId: string): Observable<User> {
    return this.teamsService.deleteTeamMember(teamId, userId).pipe(
      tap(
        user => {
          this.logger.debug('leaveTeamMembership', user);
          this.teamsStore.update(teamId, entity => ({
            members: arrayRemove(entity.members, (member: User) => member._id === userId)
          }));
        },
        error => {
          this.logger.error('leaveTeamMembership', error);
        }
      )
    );
  }
}
