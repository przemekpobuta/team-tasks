import { async, TestBed } from '@angular/core/testing';

import { AppDataAccessTeamsModule } from './app-data-access-teams.module';

describe('AppDataAccessTeamModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppDataAccessTeamsModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(AppDataAccessTeamsModule).toBeDefined();
  });
});
