import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { Logger } from '@team-tasks/app/core/logger';

import { AppDataAccessTeamsModule } from '../app-data-access-teams.module';
import { TeamsStore, TeamState } from './teams.store';

@Injectable({ providedIn: AppDataAccessTeamsModule })
export class TeamsQuery extends QueryEntity<TeamState> {
  constructor(protected store: TeamsStore, private readonly logger: Logger) {
    super(store);
  }
}
