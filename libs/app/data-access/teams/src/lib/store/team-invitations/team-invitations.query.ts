import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';

import { AppDataAccessTeamsModule } from '../../app-data-access-teams.module';
import { TeamInvitationsStore, TeamInvitationState } from './team-invitations.store';

@Injectable({ providedIn: AppDataAccessTeamsModule })
export class TeamInvitationsQuery extends QueryEntity<TeamInvitationState> {
  constructor(protected store: TeamInvitationsStore) {
    super(store);
  }
}
