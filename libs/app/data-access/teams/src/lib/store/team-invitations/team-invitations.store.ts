import { Injectable } from '@angular/core';
import { EntityState, EntityStore, StoreConfig } from '@datorama/akita';
import { Team } from '@team-tasks/shared/interfaces';

export interface TeamInvitationState extends EntityState<Team> {}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'invitations', idKey: '_id' })
export class TeamInvitationsStore extends EntityStore<TeamInvitationState> {
  constructor() {
    super();
  }
}
