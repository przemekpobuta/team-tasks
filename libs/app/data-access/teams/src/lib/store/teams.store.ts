import { Injectable } from '@angular/core';
import { ActiveState, EntityState, EntityStore, StoreConfig } from '@datorama/akita';
import { Team } from '@team-tasks/shared/interfaces';

export interface TeamState extends EntityState<Team>, ActiveState {}

const initialState = {
  active: null
};

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'teams', idKey: '_id' })
export class TeamsStore extends EntityStore<TeamState> {
  constructor() {
    super(initialState);
  }
}
