import { Injectable } from '@angular/core';
import { Logger } from '@team-tasks/app/core/logger';
import { CreateInboxTaskDto, Task, UpdateInboxTaskDto } from '@team-tasks/shared/interfaces';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { AppDataAccessInboxModule } from '../app-data-access-inbox.module';
import { InboxStore } from '../store/inbox.store';
import { InboxService } from './inbox.service';

@Injectable({ providedIn: AppDataAccessInboxModule })
export class InboxStateService {
  constructor(
    private readonly logger: Logger,
    private readonly inboxStore: InboxStore,
    private readonly inboxService: InboxService
  ) {}

  fetchInboxTasks(): void {
    this.inboxService
      .getInboxTasks()
      .pipe(
        tap(
          tasks => {
            this.logger.debug('getInboxTasks', tasks);
            this.inboxStore.set(tasks);
          },
          error => {
            this.logger.error('getInboxTasks error', error);
          }
        )
      )
      .subscribe();
  }

  createInboxTask(task: CreateInboxTaskDto): Observable<Task> {
    this.inboxStore.setLoading(true);
    return this.inboxService.createInboxTask(task).pipe(
      tap(
        (newTask: Task) => {
          this.logger.debug('createInboxTask', task);
          this.inboxStore.add(newTask);
        },
        error => {
          this.inboxStore.setLoading(false);
          this.logger.error('createInboxTask error', error);
        },
        () => {
          this.inboxStore.setLoading(false);
        }
      )
    );
  }

  updateInboxTask(taskId: string, update: Partial<UpdateInboxTaskDto>): Observable<Task> {
    return this.inboxService.updateInboxTask(taskId, update).pipe(
      tap(
        (updatedTask: Task) => {
          this.logger.debug('updateInboxTask:', updatedTask);
          this.inboxStore.replace(updatedTask._id, updatedTask);
        },
        error => {
          this.logger.error('updateInboxTask error', error);
        }
      )
    );
  }

  updateInboxTaskState(taskId: string, newCompetedValue: boolean): Observable<Task> {
    const update: Partial<UpdateInboxTaskDto> = { completed: newCompetedValue };
    return this.updateInboxTask(taskId, update);
  }

  updateInboxTaskPriority(taskId: string, newPriorityValue: boolean): Observable<Task> {
    const update: Partial<UpdateInboxTaskDto> = { priority: newPriorityValue };
    return this.updateInboxTask(taskId, update);
  }

  deleteInboxTask(taskId: string): Observable<Task> {
    return this.inboxService.deleteInboxTask(taskId).pipe(
      tap(
        (deletedTask: Task) => {
          this.logger.debug('deleteInboxTask:', deletedTask);
          this.inboxStore.remove(deletedTask._id);
        },
        error => {
          this.logger.error('deleteInboxTask error', error);
        }
      )
    );
  }

  setTaskProject(taskId: string, targetProjectId: string): Observable<Task> {
    return this.inboxService.setProject(taskId, targetProjectId).pipe(
      tap(
        (updatedTask: Task) => {
          this.logger.debug('setTaskProject update task', updatedTask);
          this.inboxStore.remove(taskId);
          this.resetActiveInboxTask();
        },
        error => {
          this.logger.error('setTaskProject error', error);
        }
      )
    );
  }

  setActiveInboxTask(taskId: string): void {
    this.inboxStore.setActive(taskId);
  }

  resetActiveInboxTask(): void {
    this.inboxStore.update({
      active: null
    });
  }

  toggleActiveInboxTask(taskId: string, state: boolean): void {
    if (state) {
      this.resetActiveInboxTask();
    } else {
      this.setActiveInboxTask(taskId);
    }
  }
}
