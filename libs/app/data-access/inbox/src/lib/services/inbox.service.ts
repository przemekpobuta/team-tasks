import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CreateInboxTaskDto, SetTaskProjectDto, Task, UpdateInboxTaskDto } from '@team-tasks/shared/interfaces';
import { environment } from 'apps/app/src/environments/environment';
import { Observable } from 'rxjs';

import { AppDataAccessInboxModule } from '../app-data-access-inbox.module';

@Injectable({ providedIn: AppDataAccessInboxModule })
export class InboxService {
  constructor(private readonly httpClient: HttpClient) {}

  getInboxTasks(): Observable<Task[]> {
    return this.httpClient.get<Task[]>(`${environment.apiUrl}/inbox`);
  }

  createInboxTask(task: CreateInboxTaskDto): Observable<Task> {
    return this.httpClient.post<Task>(`${environment.apiUrl}/inbox`, task);
  }

  updateInboxTask(taskId: string, update: Partial<UpdateInboxTaskDto>): Observable<Task> {
    return this.httpClient.put<Task>(`${environment.apiUrl}/inbox/${taskId}`, update);
  }

  deleteInboxTask(taskId: string): Observable<Task> {
    return this.httpClient.delete<Task>(`${environment.apiUrl}/inbox/${taskId}`);
  }

  setProject(taskId: string, projectId: string): Observable<Task> {
    const changeProject: SetTaskProjectDto = {
      projectId
    };
    return this.httpClient.patch<Task>(`${environment.apiUrl}/inbox/${taskId}/setProject`, changeProject);
  }
}
