import { async, TestBed } from '@angular/core/testing';
import { AppDataAccessInboxModule } from './app-data-access-inbox.module';

describe('AppDataAccessInboxModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppDataAccessInboxModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(AppDataAccessInboxModule).toBeDefined();
  });
});
