import { Injectable } from '@angular/core';
import { ActiveState, EntityState, EntityStore, StoreConfig } from '@datorama/akita';
import { Task } from '@team-tasks/shared/interfaces';

export interface InboxState extends EntityState<Task>, ActiveState {}

const initialState = {
  active: null
};

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'inbox', idKey: '_id' })
export class InboxStore extends EntityStore<InboxState> {
  constructor() {
    super(initialState);
  }
}
