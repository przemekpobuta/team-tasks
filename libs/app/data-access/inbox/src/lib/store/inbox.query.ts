import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';

import { AppDataAccessInboxModule } from '../app-data-access-inbox.module';
import { InboxState, InboxStore } from './inbox.store';

@Injectable({ providedIn: AppDataAccessInboxModule })
export class InboxQuery extends QueryEntity<InboxState> {
  constructor(protected store: InboxStore) {
    super(store);
  }
}
