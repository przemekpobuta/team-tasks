module.exports = {
  name: 'app-data-access-inbox',
  preset: '../../../../jest.config.js',
  coverageDirectory: '../../../../coverage/libs/app/data-access/inbox',
  snapshotSerializers: ['jest-preset-angular/AngularSnapshotSerializer.js', 'jest-preset-angular/HTMLCommentSerializer.js']
};
