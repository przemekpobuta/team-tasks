import { Component, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import { Logger } from '@team-tasks/app/core/logger';
import { ProjectsQuery, ProjectsStateService } from '@team-tasks/app/data-access/projects';
import { TeamsStateService } from '@team-tasks/app/data-access/teams';
import { Project } from '@team-tasks/shared/interfaces';
import { of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { ProjectShareDialogComponent } from './project-share-dialog/project-share-dialog.component';

@Component({
  selector: 'team-tasks-projects-list',
  templateUrl: './projects-list.component.html',
  styleUrls: ['./projects-list.component.scss']
})
export class ProjectsListComponent implements OnInit {
  projects$ = this.projectsQuery.selectAll();
  loading$ = this.projectsQuery.selectLoading();

  constructor(
    private readonly logger: Logger,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly projectsQuery: ProjectsQuery,
    private readonly dialogService: NbDialogService,
    private readonly teamsStateService: TeamsStateService,
    private readonly projectsStateService: ProjectsStateService
  ) {}

  ngOnInit(): void {
    this.projectsStateService.fetchProjects();
    this.teamsStateService.fetchTeams();
  }

  isProjectSelected(id: string): boolean {
    return id === this.projectsQuery.getActiveId();
  }

  onSelect(id: string): void {
    this.projectsStateService.setSelectedProject(id);
  }

  onEdit(projectId: string): void {
    this.router.navigate(['../edit', projectId], { relativeTo: this.route });
  }

  countTasks(projectId: string): number {
    return this.projectsQuery.getProjectTasksQty(projectId);
  }

  onDelete(dialog: TemplateRef<any>, project: Project): void {
    this.dialogService
      .open(dialog, { context: project.name })
      .onClose.pipe(switchMap(decision => (decision ? this.projectsStateService.deleteProject(project._id) : of())))
      .subscribe();
  }

  onShare(project: Project): void {
    this.dialogService
      .open(ProjectShareDialogComponent, { context: project.name })
      .onClose.pipe(switchMap(teamId => (teamId ? this.projectsStateService.assignProjectToTeam(project._id, teamId) : of())))
      .subscribe();
  }

  onMakePrivate(dialog: TemplateRef<any>, project: Project): void {
    this.dialogService
      .open(dialog, { context: project.name })
      .onClose.pipe(switchMap(teamId => (teamId ? this.projectsStateService.makeProjectPrivate(project._id) : of())))
      .subscribe();
  }

  get showTaskList(): boolean {
    return this.projectsQuery.hasActive();
  }
}
