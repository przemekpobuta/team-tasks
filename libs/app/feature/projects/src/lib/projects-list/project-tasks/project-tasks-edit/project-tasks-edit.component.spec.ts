import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectTasksEditComponent } from './project-tasks-edit.component';

describe('ProjectTasksEditComponent', () => {
  let component: ProjectTasksEditComponent;
  let fixture: ComponentFixture<ProjectTasksEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectTasksEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectTasksEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
