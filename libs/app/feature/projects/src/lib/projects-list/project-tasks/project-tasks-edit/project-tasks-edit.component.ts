import { Component } from '@angular/core';
import { ProjectsQuery, ProjectsStateService } from '@team-tasks/app/data-access/projects';
import { UpdateTaskDto } from '@team-tasks/shared/interfaces';

@Component({
  selector: 'team-tasks-project-tasks-edit',
  templateUrl: './project-tasks-edit.component.html'
})
export class ProjectTasksEditComponent {
  task$ = this.projectsQuery.selectProjectActiveTask();
  projects$ = this.projectsQuery.selectAll();

  get activeProjectId(): string {
    return this.projectsQuery.getActiveProjectId();
  }

  constructor(private readonly projectsQuery: ProjectsQuery, private readonly projectsStateService: ProjectsStateService) {}

  private get projectActiveTaskId(): string {
    return this.projectsQuery.getProjectActiveTaskId();
  }

  updateTask(taskUpdate: UpdateTaskDto): void {
    this.projectsStateService.updateProjectTask(this.activeProjectId, this.projectActiveTaskId, taskUpdate).subscribe();
  }

  deleteTask(): void {
    this.projectsStateService.deleteProjectTask(this.activeProjectId, this.projectActiveTaskId).subscribe();
  }

  hideSidebar(): void {
    this.projectsStateService.resetProjectActiveTask(this.activeProjectId);
  }

  changeCompleted(newCompletedValue: boolean): void {
    this.projectsStateService
      .updateProjectTaskState(this.activeProjectId, this.projectActiveTaskId, newCompletedValue)
      .subscribe();
  }

  changePriority(newPriorityValue: boolean): void {
    this.projectsStateService
      .updateProjectTaskPriority(this.activeProjectId, this.projectActiveTaskId, newPriorityValue)
      .subscribe();
  }

  changeProject(targetProjectId: string): void {
    this.projectsStateService.changeTaskProject(this.activeProjectId, targetProjectId, this.projectActiveTaskId).subscribe();
  }
}
