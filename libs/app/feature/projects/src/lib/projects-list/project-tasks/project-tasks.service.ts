import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

type SidebarOpenCloseState = 'closed' | 'open';

@Injectable()
export class ProjectTasksService {
  private sidebarState = new BehaviorSubject<SidebarOpenCloseState>('closed');

  onChangeActiveProjectTask(state: boolean): void {
    this.sidebarState.next(state ? 'open' : 'closed');
  }

  getSidebarState(): Observable<SidebarOpenCloseState> {
    return this.sidebarState.asObservable();
  }
}
