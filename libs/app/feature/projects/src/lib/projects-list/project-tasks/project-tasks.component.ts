import { transition, trigger, useAnimation } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { Logger } from '@team-tasks/app/core/logger';
import { ProjectsQuery, ProjectsStateService } from '@team-tasks/app/data-access/projects';
import { sidebarOpenClosedAnimation } from '@team-tasks/app/ui/layout';
import { CreateProjectTaskDto } from '@team-tasks/shared/interfaces';

import { ProjectTasksService } from './project-tasks.service';

@Component({
  selector: 'team-tasks-project-tasks',
  templateUrl: './project-tasks.component.html',
  styleUrls: ['./project-tasks.component.scss'],
  providers: [ProjectTasksService],
  animations: [
    trigger('sidebarOpenClosed', [
      transition(':enter', [
        useAnimation(sidebarOpenClosedAnimation, {
          params: {
            from: '0',
            to: '100%',
            timing: '150ms ease-in-out'
          }
        })
      ]),
      transition(':leave', [
        useAnimation(sidebarOpenClosedAnimation, {
          params: {
            from: '100%',
            to: '0',
            timing: '250ms ease-in-out'
          }
        })
      ])
    ])
  ]
})
export class ProjectTasksComponent implements OnInit {
  sidebarState$ = this.projectTasksService.getSidebarState();

  get showTaskList(): boolean {
    return this.projectsQuery.hasActive();
  }

  constructor(
    private readonly logger: Logger,
    private readonly projectsQuery: ProjectsQuery,
    private readonly projectsStateService: ProjectsStateService,
    private readonly projectTasksService: ProjectTasksService
  ) {}

  ngOnInit(): void {
    this.projectsQuery.hasProjectActiveTask().subscribe(value => this.projectTasksService.onChangeActiveProjectTask(value));
  }

  createNewProjectTask(createProjectTaskDto: CreateProjectTaskDto): void {
    this.logger.debug('createProjectTaskDto', createProjectTaskDto);
    this.projectsStateService.createProjectTask(this.projectsQuery.getActiveProjectId(), createProjectTaskDto).subscribe();
  }
}
