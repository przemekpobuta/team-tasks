import { Component } from '@angular/core';
import { Logger } from '@team-tasks/app/core/logger';
import { ProjectsQuery, ProjectsStateService } from '@team-tasks/app/data-access/projects';

@Component({
  selector: 'team-tasks-project-tasks-list',
  templateUrl: './project-tasks-list.component.html',
  styleUrls: ['./project-tasks-list.component.scss']
})
export class ProjectTasksListComponent {
  tasks$ = this.projectsQuery.selectActiveProjectTasks();

  constructor(
    private readonly logger: Logger,
    private readonly projectsQuery: ProjectsQuery,
    private readonly projectsStateService: ProjectsStateService
  ) {}

  onStateChange(taskId: string, newState: boolean): void {
    this.logger.debug(`onStateChange taskId: ${taskId}, newState: ${newState}`);
    this.projectsStateService.updateProjectTaskState(this.activeProjectId, taskId, newState).subscribe();
  }

  onSelectTask(taskId: string): void {
    const isSelected = this.isSelected(taskId);
    this.projectsStateService.toggleActiveProjectTask(this.activeProjectId, taskId, isSelected);
  }

  isSelected(taskId: string): boolean {
    return this.projectsQuery.getProjectActiveTaskId() === taskId;
  }

  onPriorityChange(taskId: string, newState: boolean): void {
    this.logger.debug(`onPriorityChange taskId: ${taskId}, newState: ${newState}`);
    this.projectsStateService.updateProjectTaskPriority(this.activeProjectId, taskId, newState).subscribe();
  }

  private get activeProjectId(): string {
    return this.projectsQuery.getActiveProjectId();
  }
}
