import { Component } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { TeamsQuery } from '@team-tasks/app/data-access/teams';

@Component({
  selector: 'team-tasks-project-share-dialog',
  templateUrl: './project-share-dialog.component.html',
  styleUrls: ['./project-share-dialog.component.scss']
})
export class ProjectShareDialogComponent {
  teams$ = this.teamsQuery.selectAll();

  constructor(private readonly teamsQuery: TeamsQuery, private readonly dialogRef: NbDialogRef<ProjectShareDialogComponent>) {}

  close(): void {
    this.dialogRef.close();
  }

  selectTeam(teamId: string): void {
    this.dialogRef.close(teamId);
  }
}
