import { Component } from '@angular/core';

@Component({
  selector: 'team-tasks-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent {}
