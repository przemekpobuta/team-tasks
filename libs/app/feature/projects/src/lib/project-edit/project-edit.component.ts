import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Logger } from '@team-tasks/app/core/logger';
import { ProjectsQuery, ProjectsStateService } from '@team-tasks/app/data-access/projects';
import { Project } from '@team-tasks/shared/interfaces';
import { BehaviorSubject, Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'team-tasks-project-edit',
  templateUrl: './project-edit.component.html',
  styleUrls: ['./project-edit.component.scss']
})
export class ProjectEditComponent implements OnInit {
  formGroup: FormGroup;

  get processing$(): Observable<boolean> {
    return this.processing.asObservable();
  }

  constructor(
    private readonly logger: Logger,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly formBuilder: FormBuilder,
    private readonly projectsQuery: ProjectsQuery,
    private readonly projectsStateService: ProjectsStateService
  ) {}

  private processing = new BehaviorSubject<boolean>(false);
  private id: string;

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      name: ['', Validators.required],
      description: ''
    });

    this.route.paramMap
      .pipe(switchMap((params: ParamMap) => this.projectsQuery.selectEntity(params.get('id'))))
      .subscribe(project => {
        if (project) {
          this.retriveProject(project);
        } else {
          this.projectsStateService.fetchProjects();
        }
      });
  }

  submit(): void {
    if (this.formGroup.valid) {
      this.processing.next(true);
      const formValues = this.formGroup.value;
      this.projectsStateService.updateProject(this.id, formValues).subscribe(() => {
        this.redirectToList();
        this.processing.next(false);
      });
    }
  }

  redirectToList(): void {
    this.router.navigate(['../../list'], { relativeTo: this.route });
  }

  private retriveProject(project: Project): void {
    this.id = project._id;
    this.formGroup.patchValue(project);
  }
}
