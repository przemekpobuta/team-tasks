import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProjectCreateComponent } from './project-create/project-create.component';
import { ProjectEditComponent } from './project-edit/project-edit.component';
import { ProjectsListComponent } from './projects-list/projects-list.component';
import { ProjectsComponent } from './projects.component';

export const ROUTED_COMPONENTS = [ProjectsComponent, ProjectsListComponent, ProjectCreateComponent, ProjectEditComponent];

const routes: Routes = [
  {
    path: '',
    component: ProjectsComponent,
    children: [
      {
        path: 'list',
        component: ProjectsListComponent
      },
      {
        path: 'create',
        component: ProjectCreateComponent
      },
      {
        path: 'edit/:id',
        component: ProjectEditComponent
      },
      { path: '', redirectTo: 'list', pathMatch: 'full' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppFeatureProjectsRoutingModule {}
