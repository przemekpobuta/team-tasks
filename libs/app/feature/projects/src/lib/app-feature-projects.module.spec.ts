import { async, TestBed } from '@angular/core/testing';
import { AppFeatureProjectsModule } from './app-feature-projects.module';

describe('AppFeatureProjectsModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppFeatureProjectsModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(AppFeatureProjectsModule).toBeDefined();
  });
});
