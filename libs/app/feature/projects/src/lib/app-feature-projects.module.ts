import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AppDataAccessProjectsModule } from '@team-tasks/app/data-access/projects';
import { AppDataAccessTeamsModule } from '@team-tasks/app/data-access/teams';
import { AppUiFormsModule } from '@team-tasks/app/ui/forms';
import { AppUiLayoutModule } from '@team-tasks/app/ui/layout';
import { AppUiProjectsListModule } from '@team-tasks/app/ui/projects-list';
import { AppUiTaskSidebarModule } from '@team-tasks/app/ui/task-sidebar';
import { AppUiTasksListModule } from '@team-tasks/app/ui/tasks-list';

import { AppFeatureProjectsRoutingModule, ROUTED_COMPONENTS } from './app-feature-projects-routing.module';
import { ProjectShareDialogComponent } from './projects-list/project-share-dialog/project-share-dialog.component';
import { ProjectTasksEditComponent } from './projects-list/project-tasks/project-tasks-edit/project-tasks-edit.component';
import { ProjectTasksListComponent } from './projects-list/project-tasks/project-tasks-list/project-tasks-list.component';
import { ProjectTasksComponent } from './projects-list/project-tasks/project-tasks.component';

const COMPONENTS = [ProjectTasksComponent, ProjectTasksListComponent, ProjectTasksEditComponent];
const MODAL_COMPONENTS = [ProjectShareDialogComponent];

@NgModule({
  imports: [
    CommonModule,
    AppFeatureProjectsRoutingModule,
    AppDataAccessProjectsModule,
    AppUiLayoutModule,
    AppUiFormsModule,
    AppUiProjectsListModule,
    AppUiTasksListModule,
    AppUiTaskSidebarModule,
    AppDataAccessTeamsModule
  ],
  declarations: [...ROUTED_COMPONENTS, ...COMPONENTS, ...MODAL_COMPONENTS],
  entryComponents: [...MODAL_COMPONENTS]
})
export class AppFeatureProjectsModule {}
