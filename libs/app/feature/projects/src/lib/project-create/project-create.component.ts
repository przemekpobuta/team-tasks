import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ProjectsStateService } from '@team-tasks/app/data-access/projects';
import { BehaviorSubject, Observable } from 'rxjs';

@Component({
  selector: 'team-tasks-project-create',
  templateUrl: './project-create.component.html',
  styleUrls: ['./project-create.component.scss']
})
export class ProjectCreateComponent implements OnInit {
  formGroup: FormGroup;

  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly formBuilder: FormBuilder,
    private readonly projectsStateService: ProjectsStateService
  ) {}

  private processing = new BehaviorSubject<boolean>(false);

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      name: ['', Validators.required],
      description: ''
    });
  }

  submit(): void {
    if (this.formGroup.valid) {
      this.processing.next(true);
      const formValues = this.formGroup.value;
      this.projectsStateService.createProject(formValues).subscribe(() => {
        this.router.navigate(['../list'], { relativeTo: this.route });
        this.processing.next(false);
      });
    }
  }

  get processing$(): Observable<boolean> {
    return this.processing.asObservable();
  }
}
