module.exports = {
  name: 'app-feature-projects',
  preset: '../../../../jest.config.js',
  coverageDirectory: '../../../../coverage/libs/app/feature/projects',
  snapshotSerializers: ['jest-preset-angular/AngularSnapshotSerializer.js', 'jest-preset-angular/HTMLCommentSerializer.js']
};
