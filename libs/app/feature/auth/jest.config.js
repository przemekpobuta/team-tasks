module.exports = {
  name: 'app-feature-auth',
  preset: '../../../../jest.config.js',
  coverageDirectory: '../../../../coverage/libs/app/feature/auth',
  snapshotSerializers: ['jest-preset-angular/AngularSnapshotSerializer.js', 'jest-preset-angular/HTMLCommentSerializer.js']
};
