import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { AppUiFormsModule } from '@team-tasks/app/ui/forms';
import { AppUiLayoutModule } from '@team-tasks/app/ui/layout';

import { AppFeatureAuthRoutingModule, ROUTED_COMPONENTS } from './app-feature-auth-routing.module';

@NgModule({
  imports: [CommonModule, AppFeatureAuthRoutingModule, AppUiLayoutModule, AppUiFormsModule, ReactiveFormsModule],
  declarations: [...ROUTED_COMPONENTS]
})
export class AppFeatureAuthModule {}
