import { async, TestBed } from '@angular/core/testing';
import { AppFeatureAuthModule } from './app-feature-auth.module';

describe('AppFeatureAuthModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppFeatureAuthModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(AppFeatureAuthModule).toBeDefined();
  });
});
