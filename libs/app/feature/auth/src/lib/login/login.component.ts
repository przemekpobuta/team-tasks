import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NbSidebarService } from '@nebular/theme';
import { AuthService } from '@team-tasks/app/core/auth';
import { Logger } from '@team-tasks/app/core/logger';

@Component({
  selector: 'team-tasks-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  loginForm: FormGroup;

  redirectDelay = 0;
  showMessages: any = {};
  strategy = '';

  errors: string[] = [];
  messages: string[] = [];
  user: any = {};
  submitted = false;
  rememberMe = false;

  constructor(
    private readonly logger: Logger,
    private readonly router: Router,
    private readonly formBuilder: FormBuilder,
    private readonly authService: AuthService,
    private readonly sidebarService: NbSidebarService
  ) {}

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      rememberMe: [false]
    });
    this.sidebarService.compact('left');
  }

  ngOnDestroy(): void {
    this.sidebarService.expand('left');
  }

  login(): void {
    this.errors = [];
    this.messages = [];
    this.submitted = true;
    const username = this.loginForm.get('username').value;
    const password = this.loginForm.get('password').value;

    this.authService.login(username, password).subscribe((result: any) => {
      this.submitted = false;
      this.logger.debug('login res:', result);

      // redirect to old page (query param) or to root
      const redirect = '/inbox';

      if (redirect) {
        setTimeout(() => {
          return this.router.navigateByUrl(redirect);
        }, this.redirectDelay);
      }
    });
  }
}
