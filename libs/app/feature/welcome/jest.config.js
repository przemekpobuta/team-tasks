module.exports = {
  name: 'app-feature-welcome',
  preset: '../../../../jest.config.js',
  coverageDirectory: '../../../../coverage/libs/app/feature/welcome',
  snapshotSerializers: ['jest-preset-angular/AngularSnapshotSerializer.js', 'jest-preset-angular/HTMLCommentSerializer.js']
};
