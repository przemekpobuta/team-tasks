import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AppFeatureWelcomeRoutingModule, ROUTED_COMPONENTS } from './app-feature-welcome-routing.module';

@NgModule({
  imports: [CommonModule, AppFeatureWelcomeRoutingModule],
  declarations: [...ROUTED_COMPONENTS]
})
export class AppFeatureWelcomeModule {}
