import { async, TestBed } from '@angular/core/testing';
import { AppFeatureWelcomeModule } from './app-feature-welcome.module';

describe('AppFeatureWelcomeModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppFeatureWelcomeModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(AppFeatureWelcomeModule).toBeDefined();
  });
});
