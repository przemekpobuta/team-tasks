import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '@team-tasks/app/core/auth';
import { first } from 'rxjs/operators';

@Component({
  selector: 'team-tasks-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {
  constructor(private readonly authService: AuthService, private readonly router: Router) {}

  ngOnInit() {
    this.authService
      .isAuthenticated()
      .pipe(first())
      .subscribe(isAuthenticated => {
        if (isAuthenticated) this.router.navigateByUrl('/inbox');
      });
  }
}
