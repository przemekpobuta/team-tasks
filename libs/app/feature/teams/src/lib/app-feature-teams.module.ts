import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NbDialogModule } from '@nebular/theme';
import { AppDataAccessTeamsModule } from '@team-tasks/app/data-access/teams';
import { AppUiFormsModule } from '@team-tasks/app/ui/forms';
import { AppUiLayoutModule } from '@team-tasks/app/ui/layout';
import { AppUiTeamsListModule } from '@team-tasks/app/ui/teams-list';

import { AppFeatureTeamsRoutingModule, ROUTED_COMPONENTS } from './app-feature-teams-routing.module';
import { TeamDetailsComponent } from './teams-list/team-details/team-details.component';
import { TeamInviteComponent } from './teams-list/team-details/team-invite/team-invite.component';
import { TeamInvitationsDialogComponent } from './teams-list/team-invitations-dialog/team-invitations-dialog.component';

const MODULES = [
  CommonModule,
  AppFeatureTeamsRoutingModule,
  AppDataAccessTeamsModule,
  AppUiLayoutModule,
  AppUiFormsModule,
  NbDialogModule.forChild(),
  AppUiTeamsListModule
];
const COMPONENTS = [TeamDetailsComponent];
const MODAL_COMPONENTS = [TeamInviteComponent, TeamInvitationsDialogComponent];

@NgModule({
  imports: [...MODULES],
  declarations: [...ROUTED_COMPONENTS, ...COMPONENTS, ...MODAL_COMPONENTS],
  entryComponents: [...MODAL_COMPONENTS]
})
export class AppFeatureTeamsModule {}
