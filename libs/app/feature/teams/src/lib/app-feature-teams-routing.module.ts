import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TeamCreateComponent } from './team-create/team-create.component';
import { TeamEditComponent } from './team-edit/team-edit.component';
import { TeamComponent } from './team.component';
import { TeamsListComponent } from './teams-list/teams-list.component';

export const ROUTED_COMPONENTS = [TeamComponent, TeamsListComponent, TeamCreateComponent, TeamEditComponent];

const routes: Routes = [
  {
    path: '',
    component: TeamComponent,
    children: [
      {
        path: 'list',
        component: TeamsListComponent
      },
      {
        path: 'create',
        component: TeamCreateComponent
      },
      {
        path: 'edit/:id',
        component: TeamEditComponent
      },
      { path: '', redirectTo: 'list', pathMatch: 'full' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppFeatureTeamsRoutingModule {}
