import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { TeamsQuery, TeamsStateService } from '@team-tasks/app/data-access/teams';
import { Team } from '@team-tasks/shared/interfaces';
import { BehaviorSubject, Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'team-tasks-team-edit',
  templateUrl: './team-edit.component.html'
})
export class TeamEditComponent implements OnInit {
  formGroup: FormGroup;

  get processing$(): Observable<boolean> {
    return this.processing.asObservable();
  }

  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly teamsQuery: TeamsQuery,
    private readonly formBuilder: FormBuilder,
    private readonly teamsStateService: TeamsStateService
  ) {}

  private id: string;
  private processing = new BehaviorSubject<boolean>(false);

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      name: ['', Validators.required],
      description: ''
    });

    this.route.paramMap.pipe(switchMap((params: ParamMap) => this.teamsQuery.selectEntity(params.get('id')))).subscribe(team => {
      if (team) {
        this.retriveTeam(team);
      } else {
        this.teamsStateService.fetchTeams();
      }
    });
  }

  submit(): void {
    if (this.formGroup.valid) {
      this.processing.next(true);
      const formValues = this.formGroup.value;
      this.teamsStateService.updateTeam(this.id, formValues).subscribe(() => {
        this.redirectToList();
        this.processing.next(false);
      });
    }
  }

  redirectToList(): void {
    this.router.navigate(['../../list'], { relativeTo: this.route });
  }

  private retriveTeam(team: Team): void {
    this.id = team._id;
    this.formGroup.patchValue(team);
  }
}
