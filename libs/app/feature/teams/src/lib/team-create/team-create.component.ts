import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TeamsStateService } from '@team-tasks/app/data-access/teams';
import { BehaviorSubject, Observable } from 'rxjs';

@Component({
  selector: 'team-tasks-team-create',
  templateUrl: './team-create.component.html'
})
export class TeamCreateComponent implements OnInit {
  formGroup: FormGroup;

  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly formBuilder: FormBuilder,
    private readonly teamsStateService: TeamsStateService
  ) {}

  private processing = new BehaviorSubject<boolean>(false);

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      name: ['', Validators.required],
      description: ''
    });
  }

  submit(): void {
    if (this.formGroup.valid) {
      this.processing.next(true);
      const formValues = this.formGroup.value;
      this.teamsStateService.createTeam(formValues).subscribe(() => {
        this.router.navigate(['../list'], { relativeTo: this.route });
        this.processing.next(false);
      });
    }
  }

  get processing$(): Observable<boolean> {
    return this.processing.asObservable();
  }
}
