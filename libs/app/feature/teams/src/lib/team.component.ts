import { Component, OnInit } from '@angular/core';
import { TeamsStateService } from '@team-tasks/app/data-access/teams';

@Component({
  selector: 'team-tasks-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss']
})
export class TeamComponent implements OnInit {
  constructor(private readonly teamsStateService: TeamsStateService) {}

  ngOnInit(): void {
    this.teamsStateService.fetchTeams();
    this.teamsStateService.fetchTeamInvitations();
  }
}
