import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NbDialogRef } from '@nebular/theme';
import { TeamsStateService } from '@team-tasks/app/data-access/teams';

@Component({
  selector: 'team-tasks-team-invite',
  templateUrl: './team-invite.component.html'
})
export class TeamInviteComponent implements OnInit {
  @Input() teamId: string;

  formGroup: FormGroup;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly teamsStateService: TeamsStateService,
    private readonly dialogRef: NbDialogRef<TeamInviteComponent>
  ) {}

  ngOnInit() {
    this.formGroup = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]]
    });
  }

  submit(): void {
    const email = this.formGroup.get('email').value;
    this.teamsStateService.inviteUserToTeam(this.teamId, email).subscribe(() => {
      // TODO: toastr z potwierdzeniem zaproszenia
      this.close();
    });
  }

  close(): void {
    this.dialogRef.close();
  }
}
