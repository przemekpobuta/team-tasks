import { Component, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NbDialogService, NbToastrService } from '@nebular/theme';
import { AuthQuery } from '@team-tasks/app/core/auth';
import { Logger } from '@team-tasks/app/core/logger';
import { TeamsQuery, TeamsStateService } from '@team-tasks/app/data-access/teams';
import { Team, User } from '@team-tasks/shared/interfaces';
import { Observable, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { TeamInviteComponent } from './team-invite/team-invite.component';

@Component({
  selector: 'team-tasks-team-details',
  templateUrl: './team-details.component.html',
  styleUrls: ['./team-details.component.scss']
})
export class TeamDetailsComponent {
  team$ = this.teamsQuery.selectActive();

  constructor(
    private readonly logger: Logger,
    private readonly router: Router,
    private readonly authQuery: AuthQuery,
    private readonly route: ActivatedRoute,
    private readonly teamsQuery: TeamsQuery,
    private readonly dialogService: NbDialogService,
    private readonly toastrService: NbToastrService,
    private readonly teamsStateService: TeamsStateService
  ) {}

  get showTeamDetails(): boolean {
    return this.teamsQuery.hasActive();
  }

  get team(): Team {
    return this.teamsQuery.getActive();
  }

  get teamId(): string {
    return this.teamsQuery.getActiveId().toString();
  }

  get teamOwnerId(): string {
    const teamOwner = this.team.owner as User;
    return teamOwner._id;
  }

  get isUserOwner$(): Observable<boolean> {
    return this.authQuery.selectUserId().pipe(map(userId => userId === this.teamOwnerId));
  }

  get isUserMember$(): Observable<boolean> {
    return this.isUserOwner$.pipe(map(value => !value));
  }

  addTeamMember(teamId: string): void {
    this.dialogService.open(TeamInviteComponent, { context: { teamId } }).onClose.subscribe();
  }

  deleteTeamMember(userId: string): void {
    this.teamsStateService
      .deleteTeamMember(this.teamId, userId)
      .subscribe(
        () => this.toastrService.success(`Usunięto członka zespołu`),
        err => this.toastrService.danger(`Nie udało się usunąć członka zespołu`)
      );
  }

  leaveTeam(): void {
    this.teamsStateService
      .leaveTeamMembership(this.teamId)
      .subscribe(
        () => this.toastrService.success(`Opuszczono zespół`),
        err => this.toastrService.danger(`Nie udało się opuścić zespołu`)
      );
  }

  editTeam(teamId: string): void {
    this.logger.debug(`edit teamId: ${teamId}`);
    this.router.navigate(['../edit', teamId], { relativeTo: this.route });
  }

  deleteTeam(team: Team, dialog: TemplateRef<any>): void {
    this.dialogService
      .open(dialog, { context: team.name })
      .onClose.pipe(switchMap(decision => (decision ? this.teamsStateService.deleteTeam(team._id) : of())))
      .subscribe();
  }
}
