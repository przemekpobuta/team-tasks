import { Component } from '@angular/core';
import { NbDialogRef, NbToastrService } from '@nebular/theme';
import { TeamInvitationsQuery, TeamsStateService } from '@team-tasks/app/data-access/teams';

@Component({
  selector: 'team-tasks-team-invitations-dialog',
  templateUrl: './team-invitations-dialog.component.html',
  styleUrls: ['./team-invitations-dialog.component.scss']
})
export class TeamInvitationsDialogComponent {
  invitations$ = this.teamInvitationsQuery.selectAll();

  constructor(
    private readonly toastrService: NbToastrService,
    private readonly teamsStateService: TeamsStateService,
    private readonly teamInvitationsQuery: TeamInvitationsQuery,
    private readonly dialogRef: NbDialogRef<TeamInvitationsDialogComponent>
  ) {}

  close(): void {
    this.dialogRef.close();
  }

  acceptInvitation(teamId: string): void {
    this.teamsStateService
      .acceptTeamInvitation(teamId)
      .subscribe(
        () => this.toastrService.success(`Zaakceptowano zaposzenie do zespołu`),
        err => this.toastrService.danger(`Nie udało się zaakceptować zaposzenia`)
      );
  }

  ignoreInvitation(teamId: string): void {
    this.teamsStateService
      .ignoreTeamInvitation(teamId)
      .subscribe(
        () => this.toastrService.info(`Zignorowano zaposzenie do zespołu`),
        err => this.toastrService.danger(`Nie udało się zingorować zaposzenia`)
      );
  }
}
