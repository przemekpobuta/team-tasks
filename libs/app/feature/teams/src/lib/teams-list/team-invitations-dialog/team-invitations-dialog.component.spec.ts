import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamInvitationsDialogComponent } from './team-invitations-dialog.component';

describe('TeamInvitationsDialogComponent', () => {
  let component: TeamInvitationsDialogComponent;
  let fixture: ComponentFixture<TeamInvitationsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamInvitationsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamInvitationsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
