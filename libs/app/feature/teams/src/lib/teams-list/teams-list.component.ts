import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import { TeamInvitationsQuery, TeamsQuery, TeamsStateService } from '@team-tasks/app/data-access/teams';

import { TeamInvitationsDialogComponent } from './team-invitations-dialog/team-invitations-dialog.component';

@Component({
  selector: 'team-tasks-teams-list',
  templateUrl: './teams-list.component.html',
  styleUrls: ['./teams-list.component.scss']
})
export class TeamsListComponent {
  teams$ = this.teamsQuery.selectAll();
  loading$ = this.teamsQuery.selectLoading();
  teamInvitationsCount$ = this.teamInvitationsQuery.selectCount();

  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly teamsQuery: TeamsQuery,
    private readonly dialogService: NbDialogService,
    private readonly teamsStateService: TeamsStateService,
    private readonly teamInvitationsQuery: TeamInvitationsQuery
  ) {}

  isTeamSelected(teamId: string): boolean {
    return teamId === this.teamsQuery.getActiveId();
  }

  onSelect(teamId: string): void {
    this.teamsStateService.setTeamActive(teamId);
  }

  createTeam(): void {
    this.router.navigate(['../create'], { relativeTo: this.route });
  }

  showInvitations(): void {
    this.dialogService.open(TeamInvitationsDialogComponent).onClose.subscribe();
  }
}
