import { async, TestBed } from '@angular/core/testing';

import { AppFeatureTeamsModule } from './app-feature-teams.module';

describe('AppFeatureTeamsModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppFeatureTeamsModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(AppFeatureTeamsModule).toBeDefined();
  });
});
