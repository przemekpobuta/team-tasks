module.exports = {
  name: 'app-feature-teams',
  preset: '../../../../jest.config.js',
  coverageDirectory: '../../../../coverage/libs/app/feature/teams',
  snapshotSerializers: ['jest-preset-angular/AngularSnapshotSerializer.js', 'jest-preset-angular/HTMLCommentSerializer.js']
};
