import { Component, OnInit } from '@angular/core';
import { Logger } from '@team-tasks/app/core/logger';
import { InboxQuery, InboxStateService } from '@team-tasks/app/data-access/inbox';

@Component({
  selector: 'team-tasks-inbox-tasks-list',
  templateUrl: './inbox-tasks-list.component.html',
  styleUrls: ['./inbox-tasks-list.component.scss']
})
export class InboxTasksListComponent implements OnInit {
  tasks$ = this.inboxQuery.selectAll();
  loading$ = this.inboxQuery.selectLoading();

  constructor(
    private readonly logger: Logger,
    private readonly inboxQuery: InboxQuery,
    private readonly inboxStateService: InboxStateService
  ) {}

  ngOnInit(): void {
    this.inboxStateService.fetchInboxTasks();
  }

  onStateChange(taskId: string, newState: boolean): void {
    this.logger.debug(`onStateChange taskId: ${taskId}, newState: ${newState}`);
    this.inboxStateService.updateInboxTaskState(taskId, newState).subscribe();
  }

  onSelectTask(taskId: string): void {
    this.logger.debug('onSelectTask', taskId);
    const isSelected = this.isSelected(taskId);
    this.inboxStateService.toggleActiveInboxTask(taskId, isSelected);
  }

  isSelected(taskId: string): boolean {
    return this.inboxQuery.getActiveId() === taskId;
  }
}
