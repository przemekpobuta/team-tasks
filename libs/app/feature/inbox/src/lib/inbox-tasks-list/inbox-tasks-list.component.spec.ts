import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InboxTasksListComponent } from './inbox-tasks-list.component';

describe('InboxTaskListComponent', () => {
  let component: InboxTasksListComponent;
  let fixture: ComponentFixture<InboxTasksListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InboxTasksListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InboxTasksListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
