import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InboxTaskEditComponent } from './inbox-task-edit.component';

describe('InboxTaskEditComponent', () => {
  let component: InboxTaskEditComponent;
  let fixture: ComponentFixture<InboxTaskEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InboxTaskEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InboxTaskEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
