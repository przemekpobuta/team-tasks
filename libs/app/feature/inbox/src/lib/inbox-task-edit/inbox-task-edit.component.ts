import { Component } from '@angular/core';
import { NbToastrService } from '@nebular/theme';
import { InboxQuery, InboxStateService } from '@team-tasks/app/data-access/inbox';
import { ProjectsQuery } from '@team-tasks/app/data-access/projects';
import { UpdateTaskDto } from '@team-tasks/shared/interfaces';

@Component({
  selector: 'team-tasks-inbox-task-edit',
  templateUrl: './inbox-task-edit.component.html'
})
export class InboxTaskEditComponent {
  task$ = this.inboxQuery.selectActive();
  projects$ = this.projectsQuery.selectAll();

  constructor(
    private readonly inboxQuery: InboxQuery,
    private readonly projectsQuery: ProjectsQuery,
    private readonly nbToastrService: NbToastrService,
    private readonly inboxStateService: InboxStateService
  ) {}

  private get inboxActiveTaskId(): string {
    return this.inboxQuery.getActiveId().toString();
  }

  updateTask(taskUpdate: UpdateTaskDto): void {
    this.inboxStateService.updateInboxTask(this.inboxActiveTaskId, taskUpdate).subscribe();
  }

  deleteTask(): void {
    this.inboxStateService.deleteInboxTask(this.inboxActiveTaskId).subscribe();
  }

  hideSidebar(): void {
    this.inboxStateService.resetActiveInboxTask();
  }

  changeCompleted(newCompletedValue: boolean): void {
    this.inboxStateService.updateInboxTaskState(this.inboxActiveTaskId, newCompletedValue).subscribe();
  }

  changePriority(newPriorityValue: boolean): void {
    this.inboxStateService.updateInboxTaskPriority(this.inboxActiveTaskId, newPriorityValue).subscribe();
  }

  changeProject(targetProjectId: string): void {
    const projectName = this.projectsQuery.getEntity(targetProjectId).name;
    this.inboxStateService.setTaskProject(this.inboxActiveTaskId, targetProjectId).subscribe(() => {
      this.nbToastrService.success(`Zadanie zostało przeniesione do projektu: ${projectName}`);
    });
  }
}
