import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { InboxComponent } from './inbox.component';

export const ROUTED_COMPONENTS = [InboxComponent];

const routes: Routes = [
  {
    path: '',
    component: InboxComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppFeatureInboxRoutingModule {}
