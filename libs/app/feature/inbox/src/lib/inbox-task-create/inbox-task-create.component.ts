import { Component } from '@angular/core';
import { Logger } from '@team-tasks/app/core/logger';
import { InboxStateService } from '@team-tasks/app/data-access/inbox';
import { CreateInboxTaskDto } from '@team-tasks/shared/interfaces';

@Component({
  selector: 'team-tasks-inbox-task-create',
  templateUrl: './inbox-task-create.component.html',
  styleUrls: ['./inbox-task-create.component.scss']
})
export class InboxTaskCreateComponent {
  constructor(private readonly logger: Logger, private readonly inboxStateService: InboxStateService) {}

  createNewTask(createInboxTaskDto: CreateInboxTaskDto): void {
    this.logger.debug('createNewTask', createInboxTaskDto);
    this.inboxStateService.createInboxTask(createInboxTaskDto).subscribe();
  }
}
