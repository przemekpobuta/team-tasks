import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InboxTaskCreateComponent } from './inbox-task-create.component';

describe('InboxTaskCreateComponent', () => {
  let component: InboxTaskCreateComponent;
  let fixture: ComponentFixture<InboxTaskCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InboxTaskCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InboxTaskCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
