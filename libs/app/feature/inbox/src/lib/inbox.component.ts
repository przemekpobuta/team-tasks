import { transition, trigger, useAnimation } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { InboxQuery } from '@team-tasks/app/data-access/inbox';
import { ProjectsQuery, ProjectsStateService } from '@team-tasks/app/data-access/projects';
import { sidebarOpenClosedAnimation } from '@team-tasks/app/ui/layout';

import { InboxService } from './inbox.service';

@Component({
  selector: 'team-tasks-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.scss'],
  animations: [
    trigger('sidebarOpenClosed', [
      transition(':enter', [
        useAnimation(sidebarOpenClosedAnimation, {
          params: {
            from: '0',
            to: '100%',
            timing: '150ms ease-in-out'
          }
        })
      ]),
      transition(':leave', [
        useAnimation(sidebarOpenClosedAnimation, {
          params: {
            from: '100%',
            to: '0',
            timing: '250ms ease-in-out'
          }
        })
      ])
    ])
  ]
})
export class InboxComponent implements OnInit {
  sidebarState$ = this.inboxService.getSidebarState();

  constructor(
    private readonly inboxQuery: InboxQuery,
    private readonly inboxService: InboxService,
    private readonly projectsQuery: ProjectsQuery,
    private readonly projectsStateService: ProjectsStateService
  ) {}

  ngOnInit(): void {
    this.inboxQuery.selectActiveId().subscribe(activeId => this.inboxService.onChangeActiveInboxTask(activeId as string));

    if (!this.projectsQuery.hasEntity()) {
      this.projectsStateService.fetchProjects();
    }
  }
}
