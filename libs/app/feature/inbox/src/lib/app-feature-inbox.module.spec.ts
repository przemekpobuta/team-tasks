import { async, TestBed } from '@angular/core/testing';

import { AppFeatureInboxModule } from './app-feature-inbox.module';

describe('AppFeatureInboxModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppFeatureInboxModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(AppFeatureInboxModule).toBeDefined();
  });
});
