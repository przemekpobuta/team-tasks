import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AppDataAccessInboxModule } from '@team-tasks/app/data-access/inbox';
import { AppDataAccessProjectsModule } from '@team-tasks/app/data-access/projects';
import { AppUiFormsModule } from '@team-tasks/app/ui/forms';
import { AppUiLayoutModule } from '@team-tasks/app/ui/layout';
import { AppUiTaskSidebarModule } from '@team-tasks/app/ui/task-sidebar';
import { AppUiTasksListModule } from '@team-tasks/app/ui/tasks-list';

import { AppFeatureInboxRoutingModule, ROUTED_COMPONENTS } from './app-feature-inbox-routing.module';
import { InboxTaskCreateComponent } from './inbox-task-create/inbox-task-create.component';
import { InboxTaskEditComponent } from './inbox-task-edit/inbox-task-edit.component';
import { InboxTasksListComponent } from './inbox-tasks-list/inbox-tasks-list.component';

@NgModule({
  imports: [
    CommonModule,
    AppFeatureInboxRoutingModule,
    AppUiLayoutModule,
    AppDataAccessInboxModule,
    AppUiTasksListModule,
    AppUiFormsModule,
    AppUiTaskSidebarModule,
    AppDataAccessProjectsModule
  ],
  declarations: [...ROUTED_COMPONENTS, InboxTasksListComponent, InboxTaskCreateComponent, InboxTaskEditComponent]
})
export class AppFeatureInboxModule {}
