import { Injectable } from '@angular/core';
import { Logger } from '@team-tasks/app/core/logger';
import { BehaviorSubject, Observable } from 'rxjs';

type SidebarOpenCloseState = 'closed' | 'open';

@Injectable({
  providedIn: 'root'
})
export class InboxService {
  constructor(private readonly logger: Logger) {}

  private sidebarState = new BehaviorSubject<SidebarOpenCloseState>('closed');

  onChangeActiveInboxTask(activeId: string): void {
    this.sidebarState.next(activeId ? 'open' : 'closed');
  }

  getSidebarState(): Observable<SidebarOpenCloseState> {
    return this.sidebarState.asObservable();
  }
}
