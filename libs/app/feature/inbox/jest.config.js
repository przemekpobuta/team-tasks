module.exports = {
  name: 'app-feature-inbox',
  preset: '../../../../jest.config.js',
  coverageDirectory: '../../../../coverage/libs/app/feature/inbox',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
