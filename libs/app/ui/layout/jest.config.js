module.exports = {
  name: 'app-ui-layout',
  preset: '../../../../jest.config.js',
  coverageDirectory: '../../../../coverage/libs/app/ui/layout',
  snapshotSerializers: ['jest-preset-angular/AngularSnapshotSerializer.js', 'jest-preset-angular/HTMLCommentSerializer.js']
};
