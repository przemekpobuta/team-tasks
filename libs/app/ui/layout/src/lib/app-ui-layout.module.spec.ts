import { async, TestBed } from '@angular/core/testing';
import { AppUiLayoutModule } from './app-ui-layout.module';

describe('AppUiLayoutModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppUiLayoutModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(AppUiLayoutModule).toBeDefined();
  });
});
