import { Component } from '@angular/core';
import { AuthService } from '@team-tasks/app/core/auth';

@Component({
  selector: 'team-tasks-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent {
  isAuthenticated$ = this.authService.isAuthenticated();

  constructor(private readonly authService: AuthService) {}
}
