import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'team-tasks-content-placeholder',
  templateUrl: './content-placeholder.component.html',
  styleUrls: ['./content-placeholder.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ContentPlaceholderComponent {
  @Input() text: string;
  @Input() icon = 'checkmark-circle';
}
