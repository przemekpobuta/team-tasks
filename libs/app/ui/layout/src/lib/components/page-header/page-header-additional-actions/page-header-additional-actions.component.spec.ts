import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageHeaderAdditionalActionsComponent } from './page-header-additional-actions.component';

describe('PageHeaderAdditionalActionsComponent', () => {
  let component: PageHeaderAdditionalActionsComponent;
  let fixture: ComponentFixture<PageHeaderAdditionalActionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageHeaderAdditionalActionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageHeaderAdditionalActionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
