import { Component } from '@angular/core';

@Component({
  selector: 'team-tasks-page-header-additional-actions',
  templateUrl: './page-header-additional-actions.component.html'
})
export class PageHeaderAdditionalActionsComponent {}
