import { Component, Input } from '@angular/core';

@Component({
  selector: 'team-tasks-page-header',
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.scss']
})
export class PageHeaderComponent {
  @Input() pageTitle: string;
}
