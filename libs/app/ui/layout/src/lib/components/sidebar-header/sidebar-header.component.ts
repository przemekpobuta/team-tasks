import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'team-tasks-sidebar-header',
  templateUrl: './sidebar-header.component.html',
  styleUrls: ['./sidebar-header.component.scss']
})
export class SidebarHeaderComponent {
  constructor(private readonly router: Router) {}

  navigateHome(): void {
    this.router.navigateByUrl('/');
  }
}
