import { Component } from '@angular/core';
import { NbToastrService } from '@nebular/theme';
import { AuthQuery, AuthService } from '@team-tasks/app/core/auth';

@Component({
  selector: 'team-tasks-sidebar-footer',
  templateUrl: './sidebar-footer.component.html',
  styleUrls: ['./sidebar-footer.component.scss']
})
export class SidebarFooterComponent {
  profile$ = this.authQuery.selectProfile();

  constructor(
    private readonly authQuery: AuthQuery,
    private readonly authService: AuthService,
    private readonly nbToastrService: NbToastrService
  ) {}

  logout(): void {
    this.nbToastrService.primary('Logged out!');
    this.authService.logout();
  }
}
