import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'team-tasks-container-content',
  template: `
    <ng-content></ng-content>
  `,
  styles: [
    `
      :host {
        flex: 1 1 auto;
        overflow-y: auto;
      }
    `
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ContainerContentComponent {}
