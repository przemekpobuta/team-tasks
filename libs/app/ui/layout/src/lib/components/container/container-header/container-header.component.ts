import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'team-tasks-container-header',
  template: `
    <ng-content></ng-content>
  `,
  styles: [
    `
      :host {
        flex: 0 1 auto;
      }
    `
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ContainerHeaderComponent {}
