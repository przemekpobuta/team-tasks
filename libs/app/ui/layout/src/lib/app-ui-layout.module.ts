import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import {
  NbActionsModule,
  NbAlertModule,
  NbBadgeModule,
  NbButtonModule,
  NbCardModule,
  NbContextMenuModule,
  NbDialogConfig,
  NbDialogModule,
  NbGlobalPhysicalPosition,
  NbIconModule,
  NbLayoutModule,
  NbListModule,
  NbMenuModule,
  NbPopoverModule,
  NbSidebarModule,
  NbSpinnerModule,
  NbThemeModule,
  NbToastrConfig,
  NbToastrModule,
  NbTooltipModule,
  NbUserModule,
} from '@nebular/theme';
import { AppUiFormsModule } from '@team-tasks/app/ui/forms';

import { ContainerContentComponent } from './components/container/container-content/container-content.component';
import { ContainerHeaderComponent } from './components/container/container-header/container-header.component';
import { ContainerComponent } from './components/container/container.component';
import { ContentPlaceholderComponent } from './components/content-placeholder/content-placeholder.component';
import { PageHeaderActionsComponent } from './components/page-header/page-header-actions/page-header-actions.component';
import {
  PageHeaderAdditionalActionsComponent,
} from './components/page-header/page-header-additional-actions/page-header-additional-actions.component';
import { PageHeaderComponent } from './components/page-header/page-header.component';
import { SidebarFooterComponent } from './components/sidebar-footer/sidebar-footer.component';
import { SidebarHeaderComponent } from './components/sidebar-header/sidebar-header.component';
import { LayoutComponent } from './layout.component';

const toastrConfig: Partial<NbToastrConfig> = {
  destroyByClick: true,
  duration: 3000,
  hasIcon: false,
  position: NbGlobalPhysicalPosition.BOTTOM_RIGHT
  // duplicatesBehaviour: "all",
};

const dialogConfig: Partial<NbDialogConfig> = {
  closeOnEsc: true
};

const MODULES = [CommonModule, RouterModule, AppUiFormsModule];
const GLOBAL_MODULES = [
  NbThemeModule,
  NbLayoutModule,
  NbSidebarModule,
  NbMenuModule,
  NbEvaIconsModule,
  NbIconModule,
  NbAlertModule,
  NbCardModule,
  NbActionsModule,
  NbUserModule,
  NbContextMenuModule,
  NbSpinnerModule,
  NbListModule,
  NbTooltipModule,
  NbToastrModule,
  NbButtonModule,
  NbPopoverModule,
  NbBadgeModule
];
const COMPONENTS = [SidebarHeaderComponent, SidebarFooterComponent];
const EXPORT_COMPONENTS = [
  LayoutComponent,
  PageHeaderComponent,
  PageHeaderActionsComponent,
  ContentPlaceholderComponent,
  PageHeaderAdditionalActionsComponent,
  ContainerComponent,
  ContainerHeaderComponent,
  ContainerContentComponent
];
const PROVIDERS = [
  ...NbThemeModule.forRoot({ name: 'default' }).providers,
  ...NbSidebarModule.forRoot().providers,
  ...NbMenuModule.forRoot().providers,
  ...NbToastrModule.forRoot(toastrConfig).providers,
  ...NbDialogModule.forRoot(dialogConfig).providers
];

@NgModule({
  imports: [...GLOBAL_MODULES, ...MODULES],
  exports: [...GLOBAL_MODULES, ...EXPORT_COMPONENTS],
  declarations: [...COMPONENTS, ...EXPORT_COMPONENTS]
})
export class AppUiLayoutModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: AppUiLayoutModule,
      providers: PROVIDERS
    };
  }
}
