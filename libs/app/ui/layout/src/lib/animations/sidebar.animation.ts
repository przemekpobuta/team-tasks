import { animate, animation, keyframes, style } from '@angular/animations';

export const sidebarOpenClosedAnimation = animation(
  [
    animate(
      '{{ timing }}',
      keyframes([style({ transform: 'translateX({{ to }})' }), style({ transform: 'translateX({{ from }})' })])
    )
  ],
  {
    params: {
      timing: '150ms ease-in-out'
    }
  }
);
