import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'team-tasks-task-sidebar-container',
  templateUrl: './task-sidebar-container.component.html',
  styleUrls: ['./task-sidebar-container.component.scss']
})
export class TaskSidebarContainerComponent {
  @Output() hide: EventEmitter<void> = new EventEmitter<void>();

  emitHide(): void {
    this.hide.emit();
  }
}
