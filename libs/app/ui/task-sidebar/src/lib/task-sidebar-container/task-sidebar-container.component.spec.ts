import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskSidebarContainerComponent } from './task-sidebar-container.component';

describe('TaskSidebarContainerComponent', () => {
  let component: TaskSidebarContainerComponent;
  let fixture: ComponentFixture<TaskSidebarContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaskSidebarContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskSidebarContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
