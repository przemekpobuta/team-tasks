import { async, TestBed } from '@angular/core/testing';
import { AppUiTaskSidebarModule } from './app-ui-task-sidebar.module';

describe('AppUiTaskSidebarModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppUiTaskSidebarModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(AppUiTaskSidebarModule).toBeDefined();
  });
});
