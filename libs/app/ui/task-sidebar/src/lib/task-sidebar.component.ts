import { Component } from '@angular/core';

@Component({
  selector: 'team-tasks-task-sidebar',
  templateUrl: './task-sidebar.component.html',
  styleUrls: ['./task-sidebar.component.scss']
})
export class TaskSidebarComponent {}
