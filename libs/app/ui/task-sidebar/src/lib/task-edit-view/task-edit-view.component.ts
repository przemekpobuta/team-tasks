import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  TemplateRef,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NbDialogService } from '@nebular/theme';
import { Logger } from '@team-tasks/app/core/logger';
import { Project, Task, UpdateTaskDto } from '@team-tasks/shared/interfaces';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';

@AutoUnsubscribe()
@Component({
  selector: 'team-tasks-task-edit-view',
  templateUrl: './task-edit-view.component.html',
  styleUrls: ['./task-edit-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TaskEditViewComponent implements OnInit, OnDestroy {
  @Input() set task(task: Task) {
    this._task = task;
    this.retriveTask(task);
  }
  @Input() set activeProjectId(id: string) {
    this.retriveActiveProjectId(id);
  }
  @Input() projects: Project[];
  @Output() delete: EventEmitter<void> = new EventEmitter();
  @Output() update: EventEmitter<UpdateTaskDto> = new EventEmitter();
  @Output() changeCompleted: EventEmitter<boolean> = new EventEmitter();
  @Output() changePriority: EventEmitter<boolean> = new EventEmitter();
  @Output() changeProject: EventEmitter<string> = new EventEmitter();

  editTaskFormGroup: FormGroup;

  nameChangeSubscription: Subscription;
  stateChangeSubscription: Subscription;
  projectChangeSubscription: Subscription;
  priorityChangeSubscription: Subscription;

  constructor(
    private readonly logger: Logger,
    private readonly formBuilder: FormBuilder,
    private readonly dialogService: NbDialogService
  ) {}

  private _task: Task;
  private canSaveName = new BehaviorSubject<boolean>(false);

  ngOnInit(): void {
    this.subscribeNameChange();
    this.subscribeProjectChange();
    this.subscribePriorityValueChange();
    this.subscribeCompletedValueChange();
  }

  ngOnDestroy(): void {}

  get canSaveName$(): Observable<boolean> {
    return this.canSaveName.asObservable();
  }

  deleteTask(dialog: TemplateRef<any>): void {
    this.dialogService.open(dialog, { context: this._task.name }).onClose.subscribe(decision => {
      if (decision) {
        this.logger.debug(`deleteTask ${this._task.name}`);
        this.delete.emit();
      }
    });
  }

  updateTask(): void {
    const formValues = this.editTaskFormGroup.value;

    const allowUpdateOneSave = ['name'];

    const taskUpdate = {} as UpdateTaskDto;

    Object.keys(formValues).forEach(taskKey => {
      if (allowUpdateOneSave.includes(taskKey)) {
        taskUpdate[taskKey] = formValues[taskKey];
      }
    });

    this.logger.debug('updateTask', taskUpdate);

    this.update.emit(taskUpdate);
    this.canSaveName.next(false);
  }

  private initForm(): void {
    this.editTaskFormGroup = this.formBuilder.group({
      name: ['', Validators.required],
      completed: false,
      priority: false,
      project: ['', Validators.required]
    });
  }

  private retriveTask(task: Task): void {
    if (!this.editTaskFormGroup) {
      this.initForm();
    }
    this.editTaskFormGroup.patchValue(task, { emitEvent: false });
  }

  private retriveActiveProjectId(projectId: string) {
    if (!this.editTaskFormGroup) {
      this.initForm();
    }
    this.editTaskFormGroup.get('project').setValue(projectId);
  }

  private subscribeNameChange(): void {
    this.nameChangeSubscription = this.editTaskFormGroup.get('name').valueChanges.subscribe(name => {
      this.canSaveName.next(name !== this._task.name);
    });
  }

  private subscribeCompletedValueChange(): void {
    this.stateChangeSubscription = this.editTaskFormGroup
      .get('completed')
      .valueChanges.pipe(distinctUntilChanged())
      .subscribe(newCompletedValue => {
        this.changeCompleted.emit(newCompletedValue);
      });
  }

  private subscribePriorityValueChange(): void {
    this.priorityChangeSubscription = this.editTaskFormGroup
      .get('priority')
      .valueChanges.pipe(distinctUntilChanged())
      .subscribe(newPriorityValue => {
        this.changePriority.emit(newPriorityValue);
      });
  }

  private subscribeProjectChange(): void {
    this.projectChangeSubscription = this.editTaskFormGroup
      .get('project')
      .valueChanges.pipe(distinctUntilChanged())
      .subscribe(selectedProjectId => {
        if (this.activeProjectId !== selectedProjectId) {
          this.changeProject.emit(selectedProjectId);
        }
      });
  }
}
