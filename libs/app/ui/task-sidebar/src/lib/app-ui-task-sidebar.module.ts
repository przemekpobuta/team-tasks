import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NbDialogModule } from '@nebular/theme';
import { AppUiFormsModule } from '@team-tasks/app/ui/forms';
import { AppUiLayoutModule } from '@team-tasks/app/ui/layout';

import { TaskEditViewComponent } from './task-edit-view/task-edit-view.component';
import { TaskSidebarContainerComponent } from './task-sidebar-container/task-sidebar-container.component';
import { TaskSidebarComponent } from './task-sidebar.component';

const MODULES = [CommonModule, AppUiLayoutModule, AppUiFormsModule, NbDialogModule.forChild()];
const COMPONENTS = [TaskSidebarComponent, TaskSidebarContainerComponent, TaskEditViewComponent];

@NgModule({
  imports: [...MODULES],
  declarations: [...COMPONENTS],
  exports: [...COMPONENTS]
})
export class AppUiTaskSidebarModule {}
