module.exports = {
  name: 'app-ui-task-sidebar',
  preset: '../../../../jest.config.js',
  coverageDirectory: '../../../../coverage/libs/app/ui/task-sidebar',
  snapshotSerializers: ['jest-preset-angular/AngularSnapshotSerializer.js', 'jest-preset-angular/HTMLCommentSerializer.js']
};
