import { async, TestBed } from '@angular/core/testing';
import { AppUiTeamsListModule } from './app-ui-teams-list.module';

describe('AppUiTeamsListModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppUiTeamsListModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(AppUiTeamsListModule).toBeDefined();
  });
});
