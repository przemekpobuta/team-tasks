import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamsListContainerComponent } from './teams-list-container.component';

describe('TeamsListContainerComponent', () => {
  let component: TeamsListContainerComponent;
  let fixture: ComponentFixture<TeamsListContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamsListContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamsListContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
