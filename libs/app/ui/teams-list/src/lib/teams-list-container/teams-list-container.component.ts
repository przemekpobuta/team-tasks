import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'team-tasks-teams-list-container',
  templateUrl: './teams-list-container.component.html',
  styleUrls: ['./teams-list-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TeamsListContainerComponent {
  @Input() loading: boolean;
}
