import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AppUiLayoutModule } from '@team-tasks/app/ui/layout';

import { TeamItemComponent } from './team-item/team-item.component';
import { TeamsListContainerComponent } from './teams-list-container/teams-list-container.component';

const MODULES = [CommonModule, AppUiLayoutModule];
const EXPORT_COMPONENTS = [TeamsListContainerComponent, TeamItemComponent];

@NgModule({
  imports: [...MODULES],
  declarations: [...EXPORT_COMPONENTS],
  exports: [...EXPORT_COMPONENTS]
})
export class AppUiTeamsListModule {}
