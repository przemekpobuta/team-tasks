import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { Team } from '@team-tasks/shared/interfaces';

@Component({
  selector: 'team-tasks-team-item',
  templateUrl: './team-item.component.html',
  styleUrls: ['./team-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TeamItemComponent {
  @Input() team: Team;
  @Input() selected: boolean;
  @Output() selection: EventEmitter<void> = new EventEmitter();

  selectTeam(): void {
    this.selection.emit();
  }
}
