module.exports = {
  name: 'app-ui-teams-list',
  preset: '../../../../jest.config.js',
  coverageDirectory: '../../../../coverage/libs/app/ui/teams-list',
  snapshotSerializers: ['jest-preset-angular/AngularSnapshotSerializer.js', 'jest-preset-angular/HTMLCommentSerializer.js']
};
