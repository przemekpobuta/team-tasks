module.exports = {
  name: 'app-ui-tasks-list',
  preset: '../../../../jest.config.js',
  coverageDirectory: '../../../../coverage/libs/app/ui/tasks-list',
  snapshotSerializers: ['jest-preset-angular/AngularSnapshotSerializer.js', 'jest-preset-angular/HTMLCommentSerializer.js']
};
