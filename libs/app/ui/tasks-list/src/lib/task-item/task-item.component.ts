import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Task } from '@team-tasks/shared/interfaces';
import { Subscription } from 'rxjs';

@Component({
  selector: 'team-tasks-task-item',
  templateUrl: './task-item.component.html',
  styleUrls: ['./task-item.component.scss']
})
export class TaskItemComponent implements OnInit, OnDestroy {
  taskFormGroup: FormGroup;

  stateValueChangeSubscription: Subscription;
  priorityValueChangeSubscription: Subscription;

  @Input() task: Task;
  @Input() selected: boolean;
  @Output() stateChange: EventEmitter<boolean> = new EventEmitter();
  @Output() selection: EventEmitter<void> = new EventEmitter();
  @Output() priorityChange: EventEmitter<boolean> = new EventEmitter();

  constructor(private readonly formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.taskFormGroup = this.formBuilder.group({
      state: this.task.completed,
      priority: this.task.priority
    });

    this.stateValueChangeSubscription = this.taskFormGroup
      .get('state')
      .valueChanges.subscribe(newValue => this.stateChange.emit(newValue));

    this.priorityValueChangeSubscription = this.taskFormGroup
      .get('priority')
      .valueChanges.subscribe(newValue => this.priorityChange.emit(newValue));
  }

  ngOnDestroy(): void {
    this.stateValueChangeSubscription.unsubscribe();
    this.priorityValueChangeSubscription.unsubscribe();
  }

  selectTask(): void {
    this.selection.emit();
  }
}
