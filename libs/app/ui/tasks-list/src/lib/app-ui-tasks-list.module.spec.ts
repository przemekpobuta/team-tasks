import { async, TestBed } from '@angular/core/testing';
import { AppUiTasksListModule } from './app-ui-tasks-list.module';

describe('AppUiTasksListModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppUiTasksListModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(AppUiTasksListModule).toBeDefined();
  });
});
