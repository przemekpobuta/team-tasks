import { AfterContentChecked, Component, ContentChildren, Input, QueryList } from '@angular/core';

import { TaskItemComponent } from '../task-item/task-item.component';

@Component({
  selector: 'team-tasks-tasks-list',
  templateUrl: './tasks-list.component.html',
  styleUrls: ['./tasks-list.component.scss']
})
export class TasksListComponent implements AfterContentChecked {
  @Input() loading: boolean;
  @Input() emptyPlaceholderText: string;
  @Input() emptyPlaceholderIcon: string;

  @ContentChildren(TaskItemComponent) tasks: QueryList<TaskItemComponent>;

  showContent: boolean;

  ngAfterContentChecked(): void {
    this.showContent = !!this.tasks.length;
  }
}
