import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Task } from '@team-tasks/shared/interfaces';

@Component({
  selector: 'team-tasks-task-create',
  templateUrl: './task-create.component.html',
  styleUrls: ['./task-create.component.scss']
})
export class TaskCreateComponent implements OnInit {
  @Input() loading: boolean;
  @Output() newTask: EventEmitter<Partial<Task>> = new EventEmitter();

  createTaskFormGroup: FormGroup;

  constructor(private readonly formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.createTaskFormGroup = this.formBuilder.group({
      name: ''
    });
  }

  submit(): void {
    this.newTask.emit(this.createTaskFormGroup.value);
    this.createTaskFormGroup.reset();
  }
}
