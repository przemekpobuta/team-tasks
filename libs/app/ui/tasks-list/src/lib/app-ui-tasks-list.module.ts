import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AppUiFormsModule } from '@team-tasks/app/ui/forms';
import { AppUiLayoutModule } from '@team-tasks/app/ui/layout';

import { TaskCreateComponent } from './task-create/task-create.component';
import { TaskItemComponent } from './task-item/task-item.component';
import { TasksListComponent } from './tasks-list/tasks-list.component';

const COMPONENTS = [];
const EXPORT_COMPONENTS = [TasksListComponent, TaskItemComponent, TaskCreateComponent];

@NgModule({
  imports: [CommonModule, AppUiLayoutModule, AppUiFormsModule],
  declarations: [...COMPONENTS, ...EXPORT_COMPONENTS],
  exports: [...EXPORT_COMPONENTS]
})
export class AppUiTasksListModule {}
