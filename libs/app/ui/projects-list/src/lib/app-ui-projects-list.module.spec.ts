import { async, TestBed } from '@angular/core/testing';

import { AppUiProjectsListModule } from './app-ui-projects-list.module';

describe('AppUiProjectsListModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppUiProjectsListModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(AppUiProjectsListModule).toBeDefined();
  });
});
