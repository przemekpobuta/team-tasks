import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AppUiFormsModule } from '@team-tasks/app/ui/forms';
import { AppUiLayoutModule } from '@team-tasks/app/ui/layout';

import { ProjectItemComponent } from './project-item/project-item.component';
import { ProjectsListContainerComponent } from './projects-list-container/projects-list-container.component';

const COMPONENTS = [];
const EXPORT_COMPONENTS = [ProjectsListContainerComponent, ProjectItemComponent];

@NgModule({
  imports: [CommonModule, AppUiLayoutModule, AppUiFormsModule],
  declarations: [...COMPONENTS, ...EXPORT_COMPONENTS],
  exports: [...EXPORT_COMPONENTS]
})
export class AppUiProjectsListModule {}
