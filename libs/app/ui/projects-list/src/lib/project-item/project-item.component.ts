import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Project } from '@team-tasks/shared/interfaces';

@Component({
  selector: 'team-tasks-project-item',
  templateUrl: './project-item.component.html',
  styleUrls: ['./project-item.component.scss']
})
export class ProjectItemComponent {
  @Input() project: Project;
  @Input() selected: boolean;
  @Input() tasksQty: number;
  @Output() selection: EventEmitter<void> = new EventEmitter();
  @Output() edit: EventEmitter<void> = new EventEmitter();
  @Output() delete: EventEmitter<void> = new EventEmitter();
  @Output() share: EventEmitter<void> = new EventEmitter();
  @Output() makePrivate: EventEmitter<void> = new EventEmitter();

  get isProjectShared(): boolean {
    return !!this.project.team;
  }

  selectProject(): void {
    this.selection.emit();
  }

  emitEdit(): void {
    this.edit.emit();
  }

  emitDelete(): void {
    this.delete.emit();
  }

  emitShare(): void {
    this.share.emit();
  }

  emitMakePrivate(): void {
    this.makePrivate.emit();
  }
}
