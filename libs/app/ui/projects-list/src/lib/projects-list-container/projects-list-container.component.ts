import { Component, Input } from '@angular/core';

@Component({
  selector: 'team-tasks-projects-list-container',
  templateUrl: './projects-list-container.component.html',
  styleUrls: ['./projects-list-container.component.scss']
})
export class ProjectsListContainerComponent {
  @Input() loading: boolean;
}
