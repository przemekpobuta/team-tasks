import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectsListContainerComponent } from './projects-list-container.component';

describe('ProjectsListContainerComponent', () => {
  let component: ProjectsListContainerComponent;
  let fixture: ComponentFixture<ProjectsListContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProjectsListContainerComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectsListContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
