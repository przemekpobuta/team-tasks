import { Component, forwardRef, Injector } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

import { BaseControlValueAccessor } from './../base-control-value-accessor';

@Component({
  selector: 'team-tasks-task-checkbox',
  templateUrl: './task-checkbox.component.html',
  styleUrls: ['./task-checkbox.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TaskCheckboxComponent),
      multi: true
    }
  ]
})
export class TaskCheckboxComponent extends BaseControlValueAccessor<boolean> {
  constructor(injector: Injector) {
    super(injector);
  }
}
