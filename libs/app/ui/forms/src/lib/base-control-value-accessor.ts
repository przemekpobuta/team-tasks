import { Injector, Input, OnInit, Type } from '@angular/core';
import { AbstractControl, ControlContainer, ControlValueAccessor, NgControl, ValidationErrors } from '@angular/forms';
import { Logger } from '@team-tasks/app/core/logger';

export class BaseControlValueAccessor<T> implements ControlValueAccessor, OnInit {
  @Input() label: string;
  @Input() showRequiredStar = true;
  @Input() placeholder: string;

  disabled: boolean;
  value: T;

  get isRequired() {
    return this.hasRequiredField(this.control);
  }

  get isMarkedAsInvalid(): boolean {
    return this.control.invalid && (this.control.dirty || this.control.touched);
  }

  get errors(): ValidationErrors {
    return this.control.errors;
  }

  get control(): AbstractControl {
    return this.ngControl.control;
  }

  get isFilled(): boolean {
    return Boolean(this.value);
  }

  protected ngControl: NgControl;
  protected logger: Logger;
  protected controlContainer: ControlContainer;

  constructor(private _injector: Injector) {}

  onChange(_: T): void {}

  onTouched(): void {}

  ngOnInit(): void {
    this.logger = this._injector.get<Logger>(Logger as Type<Logger>);
    this.ngControl = this._injector.get<NgControl>(NgControl as Type<NgControl>);
    this.controlContainer = this._injector.get<ControlContainer>(ControlContainer as Type<ControlContainer>);

    if (!this.controlContainer.control) {
      throw new Error('This control requires to be associated with FormGroup');
    }
  }

  writeValue(value: T): void {
    this.value = value;
  }

  isTouched(): boolean {
    return this.control.touched;
  }

  registerOnChange(fn: (value: T) => void): void {
    this.onChange = (value: T) => {
      this.value = value;
      fn(value);
    };
  }

  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  private hasRequiredField = (abstractControl: AbstractControl): boolean => {
    if (abstractControl.validator) {
      const validator = abstractControl.validator({} as AbstractControl);
      return validator && validator.required;
    }
    if (abstractControl['controls']) {
      for (const controlName in abstractControl['controls']) {
        if (abstractControl['controls'][controlName] && this.hasRequiredField(abstractControl['controls'][controlName])) {
          return true;
        }
      }
    }
    return false;
  };
}
