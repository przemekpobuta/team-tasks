import { async, TestBed } from '@angular/core/testing';
import { AppUiFormsModule } from './app-ui-forms.module';

describe('AppUiFormsModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppUiFormsModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(AppUiFormsModule).toBeDefined();
  });
});
