import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskPriorityStarComponent } from './task-priority-star.component';

describe('TaskPriorityStarComponent', () => {
  let component: TaskPriorityStarComponent;
  let fixture: ComponentFixture<TaskPriorityStarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaskPriorityStarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskPriorityStarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
