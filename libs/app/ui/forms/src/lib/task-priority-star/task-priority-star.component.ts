import { Component, forwardRef, Injector } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

import { BaseControlValueAccessor } from '../base-control-value-accessor';

@Component({
  selector: 'team-tasks-task-priority-star',
  templateUrl: './task-priority-star.component.html',
  styleUrls: ['./task-priority-star.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TaskPriorityStarComponent),
      multi: true
    }
  ]
})
export class TaskPriorityStarComponent extends BaseControlValueAccessor<boolean> {
  constructor(injector: Injector) {
    super(injector);
  }
}
