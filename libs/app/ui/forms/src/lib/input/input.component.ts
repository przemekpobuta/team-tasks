import { Component, forwardRef, Injector, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

import { BaseControlValueAccessor } from '../base-control-value-accessor';

type Size = 'small' | 'medium' | 'large';

@Component({
  selector: 'team-tasks-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputComponent),
      multi: true
    }
  ]
})
export class InputComponent extends BaseControlValueAccessor<string> {
  @Input() type = 'text';
  @Input() size: Size = 'medium';

  constructor(injector: Injector) {
    super(injector);
  }

  onMouseUp(value: any) {
    if (this.type === 'number' && value) {
      this.onChange(value);
    }
  }
}
