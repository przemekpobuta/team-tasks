import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { NbCheckboxModule, NbIconModule, NbInputModule, NbSelectModule } from '@nebular/theme';

import { InputComponent } from './input/input.component';
import { TaskCheckboxComponent } from './task-checkbox/task-checkbox.component';
import { TaskPriorityStarComponent } from './task-priority-star/task-priority-star.component';

const MODULES = [CommonModule, NbIconModule];
const GLOBAL_MODULES = [ReactiveFormsModule, NbCheckboxModule, NbCheckboxModule, NbInputModule, NbSelectModule];
const COMPONENTS = [TaskCheckboxComponent, InputComponent, TaskPriorityStarComponent];

@NgModule({
  imports: [...MODULES, ...GLOBAL_MODULES],
  declarations: [...COMPONENTS],
  exports: [...GLOBAL_MODULES, ...COMPONENTS]
})
export class AppUiFormsModule {}
