module.exports = {
  name: 'app-ui-forms',
  preset: '../../../../jest.config.js',
  coverageDirectory: '../../../../coverage/libs/app/ui/forms',
  snapshotSerializers: ['jest-preset-angular/AngularSnapshotSerializer.js', 'jest-preset-angular/HTMLCommentSerializer.js']
};
